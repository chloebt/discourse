
template<class Y>
SparseMatrix<Y>::SparseMatrix(){};

//Reads in a Kernel from file (weights are inited by file)
template<class Y>
SparseMatrix<Y>::SparseMatrix(vector<Y> const &yvals){

    filled_columns.resize(KERNEL_SIZE,false);
    columns.resize(KERNEL_SIZE);
    this->yvals = yvals;
    this->Ny = yvals.size();

    //builds a reverse action table
    unsigned int max_action_code = 0 ;
    for(int i = 0; i < this->Ny; ++i){
        uint16_t acode = this->yvals[i].get_code();
        if (acode > max_action_code){
            max_action_code = acode;
        }
    }
    
    reverse_yvals_table = vector<unsigned int> (max_action_code+1,0);
    for(int i = 0; i < this->Ny; ++i){
        reverse_yvals_table[this->yvals[i].get_code()] = i;
    }
    
}

template<class Y>
SparseMatrix<Y>::~SparseMatrix(){}

template<class Y>
SparseMatrix<Y>::SparseMatrix(vector<Y> const &yvals,string const &filename){
  load(filename,yvals);
}

//copy constructor
template<class Y>
SparseMatrix<Y>::SparseMatrix(const SparseMatrix<Y> &other){
  Ny = other.Ny;
  filled_columns = other.filled_columns;
  yvals = other.yvals;

  reverse_yvals_table.clear();
  reverse_yvals_table.resize(other.reverse_yvals_table.size());
  for (int i = 0; i  < Ny ;++i){
    reverse_yvals_table[yvals[i].get_code()] = i;
  }

  columns.resize(KERNEL_SIZE);
  for(int i = 0; i < KERNEL_SIZE;++i){
    if(filled_columns[i]){
      columns[i] = other.columns[i];
    }
  }
}
    
//assignment
template<class Y>
SparseMatrix<Y>& SparseMatrix<Y>::operator=(SparseMatrix<Y> const &other){
  
  clear();
    
  Ny = other.Ny;
  filled_columns = other.filled_columns;
  yvals = other.yvals;

  reverse_yvals_table.clear();
  reverse_yvals_table.resize(other.reverse_yvals_table.size());
  for (int i = 0; i  < Ny ;++i){
    reverse_yvals_table[yvals[i].get_code()] = i;
  }

  columns.resize(KERNEL_SIZE);
  for(int i = 0; i < KERNEL_SIZE;++i){
    if(filled_columns[i]){
      columns[i] = other.columns[i];
    }
  }
  return *this;
}

template<class Y>
void SparseMatrix<Y>::clear(){
  filled_columns.clear();
  columns.clear();
  filled_columns.resize(KERNEL_SIZE,false);
  columns.resize(KERNEL_SIZE);
}


//Loads a model from filename and erases the current model's content
template<class Y>
void SparseMatrix<Y>::load(string const &filename,vector<Y> const &yvals){

  ifstream infile;
  infile.open(filename,ios::binary);
  
  this->yvals = yvals;
  this->Ny = yvals.size();

  //Reset
  clear();
    
  unsigned int bidx=0;
    
  while(infile.read((char*)(&bidx),sizeof(unsigned int))){//Reads records
        filled_columns[bidx]=true;
        SparseFloatVector sp;
        sp.read(infile);
        columns[bidx] = sp;
    }
  infile.close();
    
  //builds reverse action table
  unsigned int max_action_code = 0 ;
  for(int i = 0; i < Ny; ++i){
    uint16_t acode = yvals[i].get_code();
    if (acode > max_action_code){
      max_action_code = acode;
    }
  }
  reverse_yvals_table = vector<unsigned int> (max_action_code+1,0);
  for(int i = 0; i < Ny; ++i){
    reverse_yvals_table[yvals[i].get_code()] = i;
  }
}

//Saves a model to filename
template<class Y>
void SparseMatrix<Y>::save(string const &filename){
 ofstream outfile;
 outfile.open(filename,ios::binary);
 //Write actual record, bucket_id+Nactions floats
 for (unsigned int i = 0; i < KERNEL_SIZE;++i){
   if (filled_columns[i]){
     outfile.write((char*) &i,sizeof(unsigned int));//bucket_id
     columns[i].write(outfile);
   }
 }
 outfile.close();
}

template<class Y>
void SparseMatrix<Y>::operator+=(const SparseMatrix &other){

  for(int i = 0; i < KERNEL_SIZE;++i){
        
    if(!filled_columns[i] && other.filled_columns[i]){
            filled_columns[i]=true;
    }
    if(other.filled_columns[i]){
      columns[i]+= other.columns[i];
    }
        //otherwise do nothing slots are both empty or the other is empty
  }
}

template<class Y>
void SparseMatrix<Y>::operator-=(const SparseMatrix &other){

  for(int i = 0; i < KERNEL_SIZE;++i){
        
    if(!filled_columns[i] && other.filled_columns[i]){
            filled_columns[i]=true;
    }
    if(other.filled_columns[i]){
      columns[i]-= other.columns[i];
    }
    //otherwise do nothing slots are both empty or the other is empty
  }

}

template<class Y>
void SparseMatrix<Y>::operator*=(float scalar){
  for(int i = 0; i < KERNEL_SIZE;++i){
        if(filled_columns[i]){
            columns[i]*= scalar;
        }
    }
}

template<class Y>
void SparseMatrix<Y>::operator/=(float scalar){
  for(int i = 0; i < KERNEL_SIZE;++i){
    if(filled_columns[i]){
      columns[i]/= scalar;
    }
  }
}

template<class Y>
SparseMatrix<Y> SparseMatrix<Y>::operator+(const SparseMatrix &other)const{
  SparseMatrix<Y> tmp(*this);
  tmp += other;
  return tmp;
}

template<class Y>
SparseMatrix<Y> SparseMatrix<Y>::operator-(const SparseMatrix &other)const{
  SparseMatrix<Y> tmp(*this);
  tmp -= other;
  return tmp;
}

template<class Y>
SparseMatrix<Y> SparseMatrix<Y>::operator*(float scalar)const{
  SparseMatrix<Y> tmp(*this);
  tmp *= scalar;
  return tmp;
}

template<class Y>
SparseMatrix<Y> SparseMatrix<Y>::operator/(float scalar)const{
  SparseMatrix<Y> tmp(*this);
  tmp /= scalar;
  return tmp;
}

template<class Y>
bool SparseMatrix<Y>::operator==(SparseMatrix const &other)const{  
  for(int i = 0; i < KERNEL_SIZE;++i){
    if(filled_columns[i]){
      if (!(columns[i] == other.columns[i])){
          return false;
      }
    }
  }
  return true;
}

/**
 * This adds the x values to the matrix Y row 
 */
template<class Y>
void SparseMatrix<Y>::addtorow(SparseFloatVector const &x_values, Y const &yval){
  unsigned int y_index = reverse_yvals_table[yval.get_code()];
  for(int i = 0; i < x_values.size();++i){
    columns[x_values.get_dense_index(i)][y_index] += x_values.get_dense_value(i);
    filled_columns[x_values.get_dense_index(i)] = true;
  }
}

/**
 * This substracts the x values to the matrix Y row 
 */
template<class Y>
void SparseMatrix<Y>::substorow(SparseFloatVector const &x_values,Y const &yval){
  unsigned int y_index = reverse_yvals_table[yval.get_code()];
  for(int i = 0; i < x_values.size();++i){
    columns[x_values.get_dense_index(i)][y_index] -= x_values.get_dense_value(i);
    filled_columns[x_values.get_dense_index(i)] = true;
  }
}

template<class Y>
ostream& SparseMatrix<Y>::watch_weights(ostream &os){
    int c = 0;
    for(int i = 0; i < KERNEL_SIZE;++i){
        if(filled_columns[i]){
            os << columns[i].to_string() << endl;
            ++c;
        }
        if (c >= 5){return os;}
    }
    return os;
}

