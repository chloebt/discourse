#        STACK                                   |   QUEUE
#     s2.t     s1.t            s0.t              |   q0         q1         q2        q3
#              /  \            /  \
#             /    \          /    \
#          s1.l    s1.r     s0.l    s0.r
#
#    t -> top
#    r -> right
#    l -> left
#
#    c  -> non-terminal      (if c is used, the last field is ignored, i.e. s0(t,c,word) and s0(t,c,tag) are equivalents)
#    h  -> head of nonterminal
#         word : word form of head
#         tag  : tag of head            (not used in these templates, as all DU have same tag)


P(1) = s0(t,h,prefw1)
P(2) = s1(t,h,prefw1)

P(3) = s0(l,h,prefw1)
P(4) = s0(r,h,prefw1)
P(5) = s1(l,h,prefw1)
P(6) = s1(r,h,prefw1)

P(7) = q0(prefw1)

P(8) = s0(t,h,prefw2)
P(9) = s1(t,h,prefw2)

P(10) = s0(l,h,prefw2)
P(11) = s0(r,h,prefw2)
P(12) = s1(l,h,prefw2)
P(13) = s1(r,h,prefw2)

P(14) = q0(prefw2)

P(15) = s0(t,h,prefw3)
P(16) = s1(t,h,prefw3)

P(17) = s0(l,h,prefw3)
P(18) = s0(r,h,prefw3)
P(19) = s1(l,h,prefw3)
P(20) = s1(r,h,prefw3)

P(21) = q0(prefw3)

P(22) = s0(t,h,prefp1)
P(23) = s1(t,h,prefp1)

P(24) = s0(l,h,prefp1)
P(25) = s0(r,h,prefp1)
P(26) = s1(l,h,prefp1)
P(27) = s1(r,h,prefp1)

P(28) = q0(prefp1)

P(29) = s0(t,h,prefp2)
P(30) = s1(t,h,prefp2)

P(31) = s0(l,h,prefp2)
P(32) = s0(r,h,prefp2)
P(33) = s1(l,h,prefp2)
P(34) = s1(r,h,prefp2)

P(35) = q0(prefp2)

P(36) = s0(t,h,prefp3)
P(37) = s1(t,h,prefp3)

P(38) = s0(l,h,prefp3)
P(39) = s0(r,h,prefp3)
P(40) = s1(l,h,prefp3)
P(41) = s1(r,h,prefp3)

P(42) = q0(prefp3)

P(43) = s0(t,h,suffw1)
P(44) = s1(t,h,suffw1)

P(45) = s0(l,h,suffw1)
P(46) = s0(r,h,suffw1)
P(47) = s1(l,h,suffw1)
P(48) = s1(r,h,suffw1)

P(49) = q0(suffw1)

P(50) = s0(t,h,suffp1)
P(51) = s1(t,h,suffp1)

P(52) = s0(l,h,suffp1)
P(53) = s0(r,h,suffp1)
P(54) = s1(l,h,suffp1)
P(55) = s1(r,h,suffp1)

P(56) = q0(suffp1)

P(57) = s0(t,h,len)
P(58) = s1(t,h,len)

P(59) = s0(l,h,len)
P(60) = s0(r,h,len)
P(61) = s1(l,h,len)
P(62) = s1(r,h,len)

P(63) = q0(len)

P(64) = s0(t,h,headw1)
P(65) = s1(t,h,headw1)

P(66) = s0(l,h,headw1)
P(67) = s0(r,h,headw1)
P(68) = s1(l,h,headw1)
P(69) = s1(r,h,headw1)

P(70) = q0(headw1)

P(71) = s0(t,h,headw2)
P(72) = s1(t,h,headw2)

P(73) = s0(l,h,headw2)
P(74) = s0(r,h,headw2)
P(75) = s1(l,h,headw2)
P(76) = s1(r,h,headw2)

P(77) = q0(headw2)

P(78) = s0(t,h,headw3)
P(79) = s1(t,h,headw3)

P(80) = s0(l,h,headw3)
P(81) = s0(r,h,headw3)
P(82) = s1(l,h,headw3)
P(83) = s1(r,h,headw3)

P(84) = q0(headw3)

P(85) = s0(t,h,headp1)
P(86) = s1(t,h,headp1)

P(87) = s0(l,h,headp1)
P(88) = s0(r,h,headp1)
P(89) = s1(l,h,headp1)
P(90) = s1(r,h,headp1)

P(91) = q0(headp1)

P(92) = s0(t,h,headp2)
P(93) = s1(t,h,headp2)

P(94) = s0(l,h,headp2)
P(95) = s0(r,h,headp2)
P(96) = s1(l,h,headp2)
P(97) = s1(r,h,headp2)

P(98) = q0(headp2)

P(99) = s0(t,h,headp3)
P(100) = s1(t,h,headp3)

P(101) = s0(l,h,headp3)
P(102) = s0(r,h,headp3)
P(103) = s1(l,h,headp3)
P(104) = s1(r,h,headp3)

P(105) = q0(headp3)

P(106) = s0(t,h,head)
P(107) = s1(t,h,head)

P(108) = s0(l,h,head)
P(109) = s0(r,h,head)
P(110) = s1(l,h,head)
P(111) = s1(r,h,head)

P(112) = q0(head)

P(113) = s0(t,h,position)
P(114) = s1(t,h,position)

P(115) = s0(l,h,position)
P(116) = s0(r,h,position)
P(117) = s1(l,h,position)
P(118) = s1(r,h,position)

P(119) = q0(position)

P(120) = s0(t,h,numb)
P(121) = s1(t,h,numb)

P(122) = s0(l,h,numb)
P(123) = s0(r,h,numb)
P(124) = s1(l,h,numb)
P(125) = s1(r,h,numb)

P(126) = q0(numb)

P(127) = s0(t,h,perc)
P(128) = s1(t,h,perc)

P(129) = s0(l,h,perc)
P(130) = s0(r,h,perc)
P(131) = s1(l,h,perc)
P(132) = s1(r,h,perc)

P(133) = q0(perc)

P(134) = s0(t,h,money)
P(135) = s1(t,h,money)

P(136) = s0(l,h,money)
P(137) = s0(r,h,money)
P(138) = s1(l,h,money)
P(139) = s1(r,h,money)

P(140) = q0(money)

P(141) = s0(t,h,date)
P(142) = s1(t,h,date)

P(143) = s0(l,h,date)
P(144) = s0(r,h,date)
P(145) = s1(l,h,date)
P(146) = s1(r,h,date)

P(147) = q0(date)

P(148)  = s0(t,c,word)             
P(149)  = s1(t,c,word)             
P(150)  = s2(t,c,word)             

P(151)  = s0(l,c,word)             
P(152)  = s0(r,c,word)             
P(153)  = s1(l,c,word)             
P(154)  = s1(r,c,word)
