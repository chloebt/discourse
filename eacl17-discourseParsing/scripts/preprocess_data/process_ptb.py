#! /usr/bin/env python

import sys
import getopt
from fastptbparser import *

"""
This is a quick made script for processing penn treebank wall street journal format and converting it to the parser input format
This kind of script is restricted to English data.
I made the choice to ignore the functional tags + empty traces
"""

"""
This defines the order of the token variables in the generated file 
"""
global_ptb_attributes = ['word','tag','lemma','trigram','suff2','suff3','suff4','ponct']

doc_vars = {'word':'the raw wordform',\
      'tag': 'the CC tag of the word',\
      'lemma':'the word lemma',\
      'trigram':'a trigram of POS of the form (c-1 c c+1)',\
      'suff2':'the 2 letter suffix of the word',
      'suff3':'the 3 letter suffix of the word',\
      'suff4':'the 4 letter suffix of the word',\
      'ponct': 'additional variable for ponct'}


def generate_doc():
    """
    generates a markdown doc of the dataset
    """

    doc_head = """
    The data sets are mainly structured as R datatables:

    * The train files include XML style markup across word lines to indicate bracketing. This turns out to be similar to the IMS CWB format.
    The train files are convertible to R datatables if the markup is removed.
    * The raw test files can be converted to R datatables provided the whitespaces are suppressed
    * The reference test files are still PTB style files without whatever morphology included (required by evalb).

    Standard split:
    
    * The train section is made of PTB (wall street journal) sections 2-21
    * The dev section is made of PTB (wall street journal) section 22
    * The test section is made of PTB (wall street journal) section 23

    The variables encoded in the Files are (from left to right, with <NA> value when the value is not provided):
    """
    doc_head+='\n'
    for att in global_ptb_attributes:
        if att in doc_vars:
            doc_head += ' '*10 +"* %s : %s"%(att,doc_vars[att]) + '\n'

    return doc_head
    




def expand_wordform(wordform,avm):
    """
    Expands a wordform by extracting suffixes and such
    @return a dict
    """
    N = len(wordform)
    if N > 2:
        avm['suff2'] = wordform[-2:]
    if N > 3:
        avm['suff3'] = wordform[-3:]
    if N > 4:
        avm['suff4'] = wordform[-4:]


def expand_trigrams(tree_root):

    tokens = tree_root.tag_yield()
    taglist = [tok[0].label for tok in tokens]
    trigrams = zip(['']+taglist[:-1],taglist,taglist[1:]+[''])
    trigrams = map(lambda t: '@'.join(t),trigrams)
    for tok,trigram in zip(tokens,trigrams):
        tok[0].trigram = trigram
    return trigrams

def expand_ponct(pos_tag,avm):

    if pos_tag in ',;()[]""':
        avm['ponct'] = 'Pw'
    elif pos_tag in '.?!':
        avm['ponct'] = 'Ps'
    else: 
        avm['ponct'] = '<NA>'

def ifelse(avm,attribute):
    """
    Values an attribute
    """
    if attribute in avm:
        return avm[attribute]
    else:
        return '<NA>'

def transform_tree(tree_root):
    """
    Manages stripping of functions and lemmatas
    """
    if tree_root.is_tag():
        tree_root.label = tree_root.label.split('-')[0]
        tree_root.label = tree_root.label.split('=')[0]
        tree_root.lemma = tree_root.children[0].label.split('^')[1]
        tree_root.children[0].label = tree_root.children[0].label.split('^')[0]
    else:
        tree_root.label = tree_root.label.split('-')[0]
        tree_root.label = tree_root.label.split('=')[0]
        for child in tree_root.children:
            transform_tree(child)

def print_tree_token(tree_root,depth):
    avm = {}
    avm['tag'] = tree_root.label
    avm['word'] = tree_root.children[0].label
    avm['trigram'] = tree_root.trigram
    avm['lemma'] = tree_root.lemma
    expand_wordform(avm['word'],avm)
    expand_ponct(avm['tag'],avm)
    fvals = [ ifelse(avm,att) for att in global_ptb_attributes]
    print (' '*(depth*2) +'\t'.join(fvals)).encode('utf-8')
    
def print_parse_tree(tree_root,depth=0):
    if tree_root.is_tag():
        print_tree_token(tree_root,depth+1)
    else:
        if tree_root.label=='':#trashes the ptb root node
            for child in tree_root.children:
                print_parse_tree(child,depth)
        else:
             lbl = tree_root.label
             print (' '*(depth*2)+'<'+lbl+'>').encode('utf-8') 
             for child in tree_root.children:
                 print_parse_tree(child,depth+1)
             print (' '*(depth*2)+'</'+lbl+'>').encode('utf-8') 

def print_raw_sent(tree_root):
    """
    prints words only
    """
    toklist = tree_root.tag_yield()
    for tag,tok in toklist:
        print_tree_token(tag,0)
    print



def process_train(inFile):

    bfr = inFile.readline()
    bfr=bfr.decode('utf-8')
    while bfr !='':
        t = parse_line(bfr)
        transform_tree(t)
        expand_trigrams(t)
        t = add_dummy_root(t)
        print_parse_tree(t)
        bfr = inFile.readline()
        bfr=bfr.decode('utf-8')
    
def process_raw(inFile):
    bfr = inFile.readline()
    bfr=bfr.decode('utf-8')
    while bfr !='':
        t = parse_line(bfr)
        transform_tree(t)
        expand_trigrams(t)
        t = add_dummy_root(t)
        print_raw_sent(t)
        bfr = inFile.readline()
        bfr=bfr.decode('utf-8')

def process_ref(inFile):

    bfr = inFile.readline()
    bfr=bfr.decode('utf-8')
    while bfr !='':
        t = parse_line(bfr)
        transform_tree(t)
        t = add_dummy_root(t)
        print t.do_flat_string().encode('utf-8')
        bfr = inFile.readline()
        bfr=bfr.decode('utf-8')


options, remainder = getopt.getopt(sys.argv[1:], 'trdg', ['--train', 
                                                         '--raw',
                                                         '--ref',
                                                         '--doc'])
generate_type = ''
for opt, arg in options:
    if opt in ('-t', '--train'):
        process_train(sys.stdin)
    elif opt in ('-r', '--raw'):
        process_raw(sys.stdin)
    elif opt in ('-g', '--ref'):
        process_ref(sys.stdin)
    elif opt in ('-d', '--doc'):
       print  generate_doc()

