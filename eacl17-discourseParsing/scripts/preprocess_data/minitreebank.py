#/usr/bin/env python

"""
A python 3 script used to generate mini treebanks from ptb head annoted treebank
"""
import sys
import LabelledTree
import fastptbparser as parser
import collections

class UnknownManager:

    def __init__(self,thr=5):
        self.counts = collections.Counter()
        self.threshold = thr

    def add_count(self,token):
        self.counts[token]+=1
        
    def map_token(self,token):
        if self.counts[token] >= self.threshold:
            return token
        else:
            return "$UNK$"
        
def as_raw(root,stream,unks):
    if root.is_tag():
        stream.write (root.children[0].label+"\t"+root.label+'\t'+unks.map_token(root.children[0].label) +'\n')
    else:
        for child in root.children:
            as_raw(child,stream,unks)
            
def as_ptb(root,stream):
    stream.write(root.do_flat_string()+'\n')

def as_native(root,stream,unks,padding=0):
    if root.is_tag():
        pad = ' ' * padding
        if root.is_head:
            stream.write(pad+root.children[0].label+"\t"+root.label+'-head'+'\t'+unks.map_token(root.children[0].label) +'\n')
        else:
            stream.write(pad+root.children[0].label+"\t"+root.label+'\t'+unks.map_token(root.children[0].label)+'\n')
    else:
        pad = ' ' * padding
        if root.is_head:
            stream.write(pad+'<'+root.label+'-head>\n')
        else:
            stream.write(pad+'<'+root.label+'>\n')
            
        for child in root.children:
            as_native(child,stream,unks,padding+3)
        stream.write(pad+'</'+root.label+'>\n')

        
def count_words(tree_root,unks):
    if tree_root.is_tag():
        unks.add_count(tree_root.children[0].label)
    else:
        for child in tree_root.children:
            count_words(child,unks)
     
def transform_tree(tree_root):
    """
    Manages stripping of functions and lemmatas
    """
    if tree_root.is_tag():
        if 'head=t' in tree_root.label:
            tree_root.is_head=True
        else:
            tree_root.is_head=False
        tree_root.label = tree_root.label.split('##')[0]
        tree_root.label = tree_root.label.split('-')[0]
        tree_root.label = tree_root.label.split('=')[0]
        #tree_root.lemma = tree_root.children[0].label.split('^')[1]
        tree_root.children[0].label = tree_root.children[0].label.split('^')[0]
    else:
        if '-head' in tree_root.label:
            tree_root.is_head = True
        else:
            tree_root.is_head=False
        tree_root.label = tree_root.label.split('-')[0]
        tree_root.label = tree_root.label.split('=')[0]
        for child in tree_root.children:
            transform_tree(child)
        
def read_corpus(filename,maxlen=20):
        inFile = open(filename)
        unks = UnknownManager()
        bfr = inFile.readline()
        treelist = []
        #print ('word\ttag\tsword')
    
        while bfr != '':
            tree = parser.parse_line(bfr)
            if len(tree.tree_yield()) < maxlen :
                transform_tree(tree)
                count_words(tree,unks)
                treelist.append(tree)
            bfr = inFile.readline()
        inFile.close()
        return (treelist,unks)

def read_french_spmrl(filename):
        unks = UnknownManager()
        trainFile = open(filename+'/ptb/train/train.French.gold.ptb.heads')
        trainlist = []
        bfr = trainFile.readline()
        while bfr != '':
            tree = parser.parse_line(bfr)
            transform_tree(tree)
            count_words(tree,unks)
            trainlist.append(tree)
            bfr = trainFile.readline()
        trainFile.close()
        
        devFile = open(filename+'/ptb/dev/dev.French.gold.ptb.heads')
        devlist = []
        bfr = devFile.readline()
        while bfr != '':
            tree = parser.parse_line(bfr)
            transform_tree(tree)
            count_words(tree,unks)
            devlist.append(tree)
            bfr = devFile.readline()
        devFile.close()
        
        testFile = open(filename+'/ptb/test/test.French.gold.ptb.heads')
        testlist = []
        bfr = testFile.readline()
        while bfr != '':
            tree = parser.parse_line(bfr)
            transform_tree(tree)
            count_words(tree,unks)
            testlist.append(tree)
            bfr = testFile.readline()
        testFile.close()
        
        return (trainlist,devlist,testlist,unks)
        

def write_header(stream):
    stream.write('word\ttag\tsword\n')

def split_dataset(treelist):
    N = len(treelist)
    chunk_size = int(round(N/10.0))
    train = treelist[:chunk_size*8]
    dev = treelist  [chunk_size*8:chunk_size*9]
    test = treelist [chunk_size*9:]
    return (train,dev,test)
    
def write_dataset(path,train,dev,test,unknowns):
    
    outFile = open(path+'/train.tbk','w')
    write_header(outFile)
    for elt in train:
        as_native(elt,outFile,unknowns)
    outFile.close()

    outFile = open(path+'/dev.tbk','w')
    outFileB = open(path+'/dev.raw','w')
    outFileC = open(path+'/dev.mrg','w')
    write_header(outFile)
    write_header(outFileB)
    for elt in dev:
        as_native(elt,outFile,unknowns)
        as_ptb(elt,outFileC)
        as_raw(elt,outFileB,unknowns)
        outFileB.write("\n")
    outFile.close()
    outFileB.close()
    outFileC.close()

    outFile = open(path+'/test.mrg','w')
    outFileB = open(path+'/test.raw','w')
    write_header(outFileB)
    for elt in test:
        as_ptb(elt,outFile)
        as_raw(elt,outFileB,unknowns)
        outFileB.write("\n")
    outFile.close()
    outFileB.close()


#treebank, unks = read_corpus(sys.argv[1],10000)
#train,dev,test = split_dataset(treebank)
train,dev,test,unks = read_french_spmrl(sys.argv[1])
unks.threshold = 2  
write_dataset('.',train,dev,test,unks)
