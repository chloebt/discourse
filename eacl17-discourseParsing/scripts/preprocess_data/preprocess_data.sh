

## Script for generating input data for the discourse parser

data=$1 #input directoy containing the data in dmrg and dependency format
parse=$2 #conll files to retrieve POS info etc
folder=$1 #outpath directory, generate tbk file for training and raw for evaluating


# 1) First the data has to be converted to dmrg and dconll format
# a- See preprocess_rst/ for dmrg for each file 

# b- TODO add the text of the EDUs into the dmrg files ie convert2dmrg-edus.py

# c- TODO add convert to dependency format ie convert2dconll.py

# 2) Get the parse information

# a- the data have to parsed using e.g. UD Pipe, files .conllu

# b- Add the index of the EDU (+ doc and sent) as a new column in the conllu files, 
# match text conllu/edus, ie addEdu2conll.py

# 2) TODO Add translations for lguages other than En
# python eacl17_code/scripts/addTranslation.py --conllu eacl17_code/data-sample/XX/parse-ud_edu/ --outpath eacl17_code/data-sample/XX/parse-ud_edu_translat/ --lang XX

# 3) Convert to tbk/raw for the parser

echo $folder $data
mkdir -p $folder

## process_pdtb.py uses dep corpus to assign heads to const corpus, then transforms const corpus in new format
## arguments:
## 1. tbk (generate tbk file) or raw (keep only tokens)
## 2. constituent corpus (mrg for discourse)
## 3. dep corpus (conll for discourse)
## 4. parse file (conll for syntax)
## output on stdout

## for train and dev datasets: generate tbk format and raw format
for c in train dev
do
    ## header of file must be the list of attributes for each token
    echo word pos prefw1 prefw2 prefw3 prefp1 prefp2 prefp3 suffw1 suffp1 len headw1 headw2 headw3 headp1 headp2 headp3 head position numb perc money date> ${folder}/${c}.tbk 
    python3 process_pdtb.py tbk ${data}constituent_format/${c}.dmrg ${data}dependency_format/${c}.dconll ${parse}/${c}.parse >> ${folder}/${c}.tbk
    
    
    echo word pos prefw1 prefw2 prefw3 prefp1 prefp2 prefp3 suffw1 suffp1 len headw1 headw2 headw3 headp1 headp2 headp3 head position numb perc money date> ${folder}/${c}.raw
    python3 process_pdtb.py raw ${data}constituent_format/${c}.dmrg ${parse}/${c}.parse >> ${folder}/${c}.raw
done


## for test generate only raw
c=test #testa/testb for Spanish

echo word pos prefw1 prefw2 prefw3 prefp1 prefp2 prefp3 suffw1 suffp1 len headw1 headw2 headw3 headp1 headp2 headp3 head position numb perc money date> ${folder}/${c}.raw
python3 process_pdtb.py raw ${data}constituent_format/${c}.dmrg ${parse}/${c}.parse >> ${folder}/${c}.raw

