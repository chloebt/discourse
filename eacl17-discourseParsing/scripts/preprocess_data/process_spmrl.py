#!/usr/bin/python

import sys
import getopt
import StringIO

from fastptbparser import *

"""
This is a script processing SPMRL format and converting it to the parser input format
This kind of script could in principle be made general for any SPMRL language, but it currently is restricted to French data.
I made the choice to ignore the compound marks for now (compounds are kept as different tokens), as well as trashing out functional tags
"""

"""
This defines the order of the token variables in the generated file 
"""
global_spmrl_attributes = ['word','pos','lem','cpos','trigram','s','g','n','m','t','p','mwehead','suff2','suff3','suff4','ponct']

doc_vars ={'word':'the raw wordform',\
      'pos': 'the CC tag of the word',\
      'lem':'the word lemma',\
      'cpos':'the raw pos of the word',\
      'trigram':'a trigram of POS of the form (c-1 c c+1)',\
      's': 'the subcat of the word',
      'g': 'the gender of the word',
      'n': 'the number of the word',
      'm': 'the mood of the word',
      't': 'the tense of the word',
      'p': 'the person of the word',
      'mwehead' : 'says if this word is the head of a multiword expression',
      'suff2':'the 2 letter suffix of the word',
      'suff3':'the 3 letter suffix of the word',\
      'suff4':'the 4 letter suffix of the word',\
      'ponct': 'additional variable for ponct'}


def generate_doc():
    """
    generates a markdown doc of the dataset
    """

    doc_head = """
    The data sets are mainly structured as R datatables:

    * The train files include XML style markup across word lines to indicate bracketing. This turns out to be similar to the IMS CWB format.
    The train files are convertible to R datatables if the markup is removed.
    * The raw test files can be converted to R datatables provided the whitespaces are suppressed
    * The reference test files are still PTB style files without whatever morphology included (required by evalb).

    The variables encoded in the Files are (from left to right, with <NA> value when the value is not provided):
    """
    doc_head+='\n'
    for att in global_spmrl_attributes:
        if att in doc_vars:
            doc_head += ' '*10 +"* %s : %s"%(att,doc_vars[att]) + '\n'

    return doc_head
    
def ifelse(avm,attribute):
    """
    Values an attribute
    """
    if attribute in avm:
        return avm[attribute]
    else:
        return '<NA>'

def split_att_val(av):
    """
    There is a confusion between metachars and chars in spmrl format, patching it
    """
    if av[-2]=='=' and av[-1]=='=':
        return (av[0],av[-1])
    else:
        #print av,av.replace('==','=').split('=')
        return av.replace('==','=').split("=")

def avm_fix(avm):
    if 'cpos' in avm and avm['cpos'] == '':
        avm['cpos'] = 'X'
    return avm
    
def expand_wordform(wordform,avm):
    """
    Expands a wordform by extracting suffixes and such
    @return a dict
    """
    N = len(wordform)
    if N > 2:
        avm['suff2'] = wordform[-2:]
    if N > 3:
        avm['suff3'] = wordform[-3:]
    if N > 4:
        avm['suff4'] = wordform[-4:]

def expand_ponct(pos_tag,avm):
    
    if pos_tag =='PONCT' and 's' in avm and avm['s'] == 'w':
        avm['ponct'] = 'Pw'
    elif pos_tag == 'PONCT' and 's' in avm and avm['s'] == 's':
        avm['ponct'] = 'Ps'
    else: 
        avm['ponct'] = '<NA>'

def expand_trigrams(tree_root):
    tokens = tree_root.tag_yield()
    taglist = [tok[0].label.split('#')[0].split('-')[0] for tok in tokens]
    trigrams = zip(['']+taglist[:-1],taglist,taglist[1:]+[''])
    trigrams = map(lambda t: '@'.join(t),trigrams)
    for tok,trigram in zip(tokens,trigrams):
        tok[0].trigram = trigram
    return trigrams

def print_tree_token(tree_root,depth):
    pos_tag,e,features,f,g = tree_root.label.split('#')
    if '-' in pos_tag:
        pos_tag = pos_tag.split('-')[0]
    wordform = tree_root.children[0].label
    flist = features.split('|')
    flist = filter(lambda x: x != '_',flist)      #trashes useless _
    avm = dict([split_att_val(feat) for feat in flist])
    #fixes bugs in SPMRL dataset
    avm = avm_fix(avm) 
    expand_wordform(wordform,avm)
    expand_ponct(pos_tag,avm)
    avm['word'] = wordform
    avm['pos'] = pos_tag
    avm['trigram'] = tree_root.trigram
    fvals = [ ifelse(avm,att) for att in global_spmrl_attributes]
    return (' '*(depth*2) +'\t'.join(fvals)).encode('utf-8')

def strip_tags(tree_root):
    """
    Strips the features from the tags
    """
    if tree_root.is_tag():
        tree_root.label = tree_root.label.split('#')[0]
        tree_root.label = tree_root.label.split('-')[0]
    else :
        if tree_root.label != '':
            tree_root.label = tree_root.label.split('-')[0]
        for child in tree_root.children:
            strip_tags(child)
        
def print_parse_tree(tree_root,depth=0):
    s =''
    if tree_root.is_tag():
        s+= print_tree_token(tree_root,depth+1)+'\n'
    else:
        if tree_root.label=='':#trashes the ptb root node
            for child in tree_root.children:
                s+= print_parse_tree(child,depth)
        else:
             lbl = tree_root.label.split('-')[0] #trashes func tags
             s += (' '*(depth*2)+'<'+lbl+'>\n').encode('utf-8') 
             for child in tree_root.children:
                 s += print_parse_tree(child,depth+1)
             s += (' '*(depth*2)+'</'+lbl+'>\n').encode('utf-8') 
    return s

def print_raw_sent(tree_root):
    """
    prints words only
    """
    toklist = tree_root.tag_yield()
    sent = '\n'.join([print_tree_token(tag,0) for tag,tok in toklist])
    return sent

def process_train(inFile,outFile=None):

    bfr = inFile.readline()
    bfr=bfr.decode('utf-8')
    while bfr !='':
        t = parse_line(bfr)
        expand_trigrams(t)
        if outFile != None:
            outFile.write(print_parse_tree(t))
        else:
            print_parse_tree(t)
        bfr = inFile.readline()
        bfr=bfr.decode('utf-8')

def process_raw_ptb(inFile,outFile=None):
    bfr = inFile.readline()
    bfr=bfr.decode('utf-8')
    while bfr !='':
        t = parse_line(bfr)
        if outFile != None:
            outFile.write(t.do_sentence_string().encode('utf-8')+'\n')
        else:
            print t.do_sentence_string().encode('utf-8')
        bfr = inFile.readline()
        bfr=bfr.decode('utf-8')
        
def process_raw(inFile,outFile=None):

    bfr = inFile.readline()
    bfr=bfr.decode('utf-8')
    while bfr !='':
        t = parse_line(bfr)
        expand_trigrams(t)
        if outFile != None:
            outFile.write(print_raw_sent(t)+'\n\n')
        else:
            print print_raw_sent(t)
        bfr = inFile.readline()
        bfr=bfr.decode('utf-8')
    
def process_ref(inFile,outFile=None):

    bfr = inFile.readline()
    bfr=bfr.decode('utf-8')
    while bfr !='':
        t = parse_line(bfr)
        strip_tags(t)
        if outFile != None:
            outFile.write((t.do_flat_string()+'\n').encode('utf-8'))
        else:
            print t.do_flat_string().encode('utf-8')
        bfr = inFile.readline()
        bfr=bfr.decode('utf-8')

def process_all(inFile):

    all_buff = inFile.read()
    buff = StringIO.StringIO(all_buff)
    
    treebankRaw = open('treebank.raw','w')
    process_raw(buff,treebankRaw)
    treebankRaw.close()
    buff.close()

    buff = StringIO.StringIO(all_buff)
    treebankRef = open('treebank.mrg','w')
    process_ref(buff,treebankRef)
    treebankRef.close()
    buff.close()
    
    buff = StringIO.StringIO(all_buff)
    treebankTrain = open('treebank.tbk','w')
    process_train(buff,treebankTrain)
    treebankTrain.close()
    buff.close()

    buff = StringIO.StringIO(all_buff)
    treebankPTB = open('treebank.raw2','w')
    process_raw_ptb(buff,treebankPTB)
    treebankPTB.close()
    buff.close()
    
options, remainder = getopt.getopt(sys.argv[1:], 'trdga', ['--train', 
                                                         '--raw',
                                                         '--ref',
                                                         '--doc',
                                                         '--all'])
generate_type = ''
for opt, arg in options:
    if opt in ('-t', '--train'):
        process_train(sys.stdin)
    elif opt in ('-r', '--raw'):
        process_raw(sys.stdin)
    elif opt in ('-g', '--ref'):
        process_ref(sys.stdin)
    elif opt in ('-a', '--all'):
        process_all(sys.stdin)
    elif opt in ('-d', '--doc'):
       print  generate_doc()
