#!/usr/bin/env python

import sys

"""
This script strips syntactic annotations from a .tbk file
usage cat file | python strip_syntax.py > stripped.file.raw
"""

def strip_file(instream):
    bfr = instream.readline()
    while bfr != '':
        bfr = bfr.strip()
        #print '*',bfr
        if bfr[0] != '<' and (bfr[-1] != '>' or bfr[-4:] == '<NA>'):
            print '\t'.join(bfr.split())
            #print bfr
            #print bfr.split()[0]
        bfr = instream.readline()
            
inFile = sys.stdin
strip_file(inFile)
