#!/usr/bin/env python3 -O

import sys
import getopt
from collections import *

"""
This is a generic data transformation script made to transform a .tbk style file format into a trans.tbk file.
This is meant to be customized/extended for specific experimental purposes.

The script is organized around two classes:
    the LocalTransformer adds a column by using informations local to a token
    the GlobalTransformer adds a column by using informations global to the whole sentence

    One may design additional transformers to customize the transformation process and add new columns
    by subclassing these two abstract classes.
"""

class LocalTransformer(object):
    
    def __init__(self):
        pass

    def is_relevant(self,varnames):
        """
        Checks if the vars transformed by this transformer do actually exist in the dataset
        """
        raise NotImplementedError("Should have implemented the type checking function") 
    
    def name(self):
        raise NotImplementedError("Should have implemented the naming function") 
     
    """
    Value generator
    @param varnames: a list of base variable names (not variables generated during transformations) allowing to map var names to indexes
    @param columns : a list of values for each variable (same size as varnames)
    @return a value for this transformer
    """
    def generate_value(self,varnames,columns):
        raise NotImplementedError( "Should have implemented the valuation function")

       
class GlobalTransformer(object):

    def __init__(self):
        pass

    def name(self):
        raise NotImplementedError("Should have implemented the naming function")
     
    """
    Value generator
    @param varnames: a list of base variable names (not variables generated during transformations) allowing to map var names to indexes
    @param columns : a matrix NxD of values (for each line 0<=i<N for each variable 0<=j<D)
    @return a list of N values, one for each token in the sentence
    """
    def generate_values(self,varnames,columns):
        raise NotImplementedError( "Should have implemented the valuation function")


    
class SmoothCounter:
    """
    This counter, counts the values distribution for this varname
    @param varname: the var to count for
    @param all_varnames: the ordered list of variables used in this dataset
    """
    def __init__(self,varname,all_varnames):
        self.var_idx = all_varnames.index(varname)
        self.var_dic = Counter()
        
    def count(self,filename):
        """
        Builds a frequency dictionary from a treebank file.
        """
        inFile = open(filename)
        bfr = inFile.readline()
        while bfr != '':
            tbfr = bfr.strip()
            if not tbfr.isspace()  and not tbfr == '' and not (tbfr[0] == '<' and tbfr[-1]=='>'):
                fields = tbfr.split()
                self.var_dic[fields[self.var_idx]] += 1
            bfr = inFile.readline()
        inFile.close()
        return self.var_dic

    def aboveEq_threshold(self,threshold=0):
        """
        Returns a subCounter with elements above or eq to threshold only
        """
        return Counter(dict([(elt,cnt) for (elt,cnt)  in self.var_dic.items() if cnt >= threshold]))
 
def is_number(wordform):
    """
    Returns true if this string is a sequence of digits only
    """
    return wordform.replace('/','').replace('_','').replace(',','').replace('.','').isdigit()

class LocalLexicalSmoother(LocalTransformer):

    def __init__(self,varname,varnames,word_counter):
        self.word_counter = word_counter
        self.var_idx = varnames.index(varname)

    def is_relevant(self,varnames):
        return True
    
    def name(self):
        return 'sword'

    def generate_value(self,varnames,columns):
        wordform = columns[self.var_idx]
        if is_number(wordform):
            return '**NUM**'
        elif columns[self.var_idx] in self.word_counter:
            return columns[self.var_idx]
        else:
            return '**UNK**'

class LocalClusterTransformer(LocalTransformer):

    def __init__(self,varname,varnames,cluster_filename):
        self.var_idx = varnames.index(varname)
        self.__name = 'clust'
        self.__read_cluster_file(cluster_filename)
        
    def __read_cluster_file(self,filename):
        self.cdict = {}
        clIn = open(filename)
        bfr = clIn.readline()
        while bfr != '' and not bfr.isspace():
            (clst,word,freq) = bfr.split('\t')
            if '<' in word:
                word = word.replace('<','&lt;')
            if '>' in word:
                word = word.replace('>','&gt;')
            self.cdict[word] = clst
            bfr = clIn.readline()
        clIn.close()
        
    def is_relevant(self,varnames):
        return True

    def name(self):
        return self.__name

    def generate_value(self,varnames,columns):
        word = columns[self.var_idx]
        if word in self.cdict:
            return self.cdict[word]
        else:
            return 'NA'
            
        
class DummyVariableAdder(LocalTransformer):
        
        def __init__(self,varname,varnames):
                self.var_idx = varnames.index(varname)
                self.__name = str(varname)
        def is_relevant(self,varnames):
                return True
        def name(self):
                return self.__name
        
        def generate_value(self,varnames,columns):
                return 'NA'

        
class LocalLexTagSmoother(LocalTransformer):
    """
    Replaces low freq words by their tags
    """
    def __init__(self,varname,varnames,word_counter):
        self.word_counter = word_counter
        self.var_idx = varnames.index(varname)

    def is_relevant(self,varnames):
        return True
    
    def name(self):
        return 'wordXtag'

    def generate_value(self,varnames,columns):
        wordform = columns[self.var_idx]
        if is_number(wordform):
            return '**NUM**'
        elif columns[self.var_idx] in self.word_counter:
            return columns[self.var_idx]
        else:
            tag = columns[varnames.index('tag')]
            if '-' in tag:
                tag = tag.split('-')[0]
            return tag
        
class LocalSuffixSmoother(LocalTransformer):

    def __init__(self,varname,varnames,sufflen=3):
        self.var_idx = varnames.index(varname)
        self.sufflen = sufflen
        
    def name(self):
        return 'suff'+str(self.sufflen)

    def is_relevant(self,varnames):
        return True
    
    def generate_value(self,varnames,columns):
        wordform = columns[self.var_idx]
        if is_number(wordform):
            return '**NUM**'
        else:
            return wordform[-self.sufflen:]

class AgreementGenerator(LocalTransformer):

    def __init__(self):
        pass

    def name(self):
        return 'agr'

    def is_relevant(self,varnames):
        return 'gen' in varnames and 'num' in varnames
    
    def generate_value(self,varnames,columns):
        return columns[varnames.index('gen')]+columns[varnames.index('num')]

    
class GlobalLexicalSmoother(GlobalTransformer):

    def __init__(self,varname,varnames,word_counter):
        self.word_counter = word_counter
        self.var_idx = varnames.index(varname)
        
    def name(self):
        return 'nsword'

    #removes functional annotations from tags
    def strip_tag(self,tag):
        if '-' in tag:
            return tag.split('-')[0]
        else:
            return tag
        
    def generate_values(self,varnames,columns):
        pos_idx = varnames.index('tag')
        pos_list = list([line[pos_idx] for line in columns])
       
        vals = list([line[self.var_idx] for line in columns])
        pos_list = ['XXX']+pos_list
        for idx,line in enumerate(columns):
            wordform = line[self.var_idx]            
            if is_number(wordform):
                vals[idx] = '**NUM**'
            elif line[self.var_idx] in self.word_counter:
                pass
            else:
                vals[idx] = self.strip_tag(pos_list[idx])+'_'
        return vals
"""
Generates pos trigrams around the word at index i such that (pos[i-1],pos[i],pos[i+1])
"""
class PosNgramGenerator(GlobalTransformer):
    
    def __init__(self,k=3):
        self.var_idx = 1#pos have index = 1
        assert(k in [3,5,7,9])
        self.K = k
        
    def name(self):
        if self.K == 3:
            return 'trigram'
        elif self.K == 5:
            return 'pentagram'
        elif self.K == 7:
            return 'heptagram'
        elif self.K == 9:
            return 'enneagram'
            
    #removes functional annotations from tags
    def strip_tag(self,tag):
        if '-' in tag:
            return tag.split('-')[0]
        else:
            return tag
    # """
    # Currently generates trigrams
    # """
    # def generate_values(self,varnames,columns):
    #     pos_list = list([self.strip_tag(line[self.var_idx]) for line in columns])
    #     before_list = ['**S**']+pos_list[:-1]
    #     after_list = pos_list[1:]+['**E**']
    #     return list(['%s+%s+%s'%(p1,p2,p3) for (p1,p2,p3) in zip(before_list,pos_list,after_list)])

    """
    Generates k-grams centered around the word
    """
    def generate_values(self,varnames,columns):
        pos_list = list([self.strip_tag(line[self.var_idx]) for line in columns])
        shift = int( (self.K-1)/2 )
        all_lists = [ ]
        for k in reversed(range(1,shift+1)):
            before_list = k*['**S**']+pos_list[:-k]
            all_lists.append(before_list)
        all_lists.append(pos_list)
        for k in range(1,shift+1):
            after_list = pos_list[k:] + k*['**E**']
            all_lists.append(after_list)
        return list( ['+'.join(elt) for elt in zip(*all_lists)])
            
class Transformer(object):

    def __init__(self,train_filename,threshold=3):
        self.trainfile = train_filename
        self.local_generators  = []
        self.global_generators = []
        self.varnames = self.__get_varnames(train_filename)
        wsc = SmoothCounter('word',self.varnames)
        wsc.count(train_filename)
        self.word_dic = wsc.aboveEq_threshold(threshold)

    def __get_varnames(self,filename):
        inFile = open(filename)
        bfr = inFile.readline()
        return bfr.split()
        inFile.close()

    def get_varnames(self):
        return self.varnames
        
    def get_word_counts(self):
        return self.word_dic
    
    def add_local_generator(self,generator):
        if generator.is_relevant(self.varnames):
            self.local_generators.append(generator)
        
    def add_global_generator(self,generator):
        self.global_generators.append(generator)

    def process_file(self,filename):
        in_file = open(filename)
        outfile = open(filename[:-3]+'trans.'+filename[-3:],'w')
        outfile.write('\t'.join(self.varnames+[g.name() for g in self.local_generators]+[g.name() for g in self.global_generators])+'\n') #writes extended header
        sentence  = [ ]
        sent_view = [ ]
        arity = len(self.varnames)
        bfr = in_file.readline()       #skips header
        bfr = in_file.readline()
        while bfr != '':            
            if bfr != '' and not bfr.isspace():
                sentence.append(bfr)
                bfr = bfr.strip()
                fields = bfr.split()
                if len(fields) == arity:
                    sent_view.append(fields)  #keeps a view on lexical fields only                    
            else:
                if len(sentence) > 0:
                    up_view = self.__process_sentence(sent_view)
                    self.__merge_view(outfile,sent_view,sentence,arity)
                sentence  = [ ]
                sent_view = [ ] 
            bfr = in_file.readline()
        if len(sentence) > 0:
            self.__process_sentence(sentence)
            self.__merge_view(outfile,sent_view,sentence,arity)
        in_file.close()
        outfile.close()
        
    def __process_sentence(self,sent_view):
        #Applies local generators
        for line in sent_view:
            line.extend([g.generate_value(self.varnames,line) for g in self.local_generators])
            
        #Applies global generators
        next_cols = []
        for g in self.global_generators:
            next_cols.append(g.generate_values(self.varnames,sent_view))
        for idx,line in enumerate(sent_view):
            for elt in next_cols:
                line.append(elt[idx])
        return sent_view

    def __merge_view(self,outfile,sent_view,sent,arity):
        view_idx = 0
        for line in sent:
            if len(line.split()) == arity :
                outfile.write(' '*self.__count_lwsp(line)+'\t'.join(sent_view[view_idx])+'\n')
                view_idx += 1
            else:
                outfile.write(line)
        outfile.write('\n')
         
    #counts leading whitespaces
    def __count_lwsp(self,line):
        nwsp = 0
        for c in line:
            if c == ' ':
                nwsp+=1
            else:
                return nwsp

            
def process_scenario(name):
    assert(name in ['pred','gold'])
    
    #Script to be called from within its directory (skeletons) 
    train_filename = '../%s/ptb/train/treebank.tbk'%(name,)
    dev_filename = '../%s/ptb/dev/treebank.tbk'%(name,)
    #test_filename = '../%s/ptb/test/treebank.raw'%(name,)
    sptest_filename = '../%s/ptb/test/treebank.raw'%(name,)

    rtrain_filename = '../%s/ptb/train/treebank.tbk'%(name,)
    rdev_filename = '../%s/ptb/dev/treebank.raw'%(name,)
    #rtest_filename = '../%s/ptb/test/treebank.raw'%(name,)
    sprtest_filename = '../%s/ptb/test/treebank.raw'%(name,)

    clust_filename = 'clusts.txt'

    
    T = 2#Lexical smoother threshold
    tr = Transformer(train_filename,threshold=T)
    vnames = tr.get_varnames()
    wc = tr.get_word_counts()
    #Basic word smoothers (smoothing on word column) 
    tr.add_local_generator(LocalLexicalSmoother('word',vnames,wc))
    tr.add_local_generator(LocalLexTagSmoother('word',vnames,wc))
    tr.add_local_generator(LocalSuffixSmoother('word',vnames,sufflen=3))
    tr.add_local_generator(AgreementGenerator())
    tr.add_local_generator(LocalClusterTransformer('word',vnames,clust_filename))

    #Ngrams smoothers
    tr.add_global_generator(PosNgramGenerator(k=3))
    tr.add_global_generator(PosNgramGenerator(k=5))
    tr.add_global_generator(GlobalLexicalSmoother('word',vnames,wc))
    #Variable normalizer
    varset = set(vnames)
    required_vars = ['subcat','mood','gen','num','case','lem']
    vnames = vnames[:]
    for var in required_vars:
        if var not in varset:
            vnames.append(var)
            
    for var in required_vars:
        if var not in varset:
            tr.add_local_generator(DummyVariableAdder(var,vnames))
    
    #Run
    tr.process_file(train_filename)
    tr.process_file(dev_filename)
    #tr.process_file(test_filename)
    tr.process_file(sptest_filename)
    tr.process_file(rtrain_filename)
    tr.process_file(rdev_filename)
    #tr.process_file(rtest_filename)
    tr.process_file(sprtest_filename)

        
if __name__ == '__main__':
    process_scenario('gold')
    process_scenario('pred')
