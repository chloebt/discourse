# README #

This repository contains the code necessary to replicate the experiments presented in: 

    C. Braud, M. Coavoux and A. S�gaard. Cross-lingual RST Discourse Parsing, In Proceedings of EACL. 2017
    
## DISCOURSE PARSER

The code of the parser is in hyparse_fork/. It has to be compiled:
    
    cd hyparse_fork
    mkdir build
    cd src2/dense_parser
    make
    
The parser uses [boost](http://www.boost.org/) and [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) libraries.
If they are not installed on your system, run the following lines before compiling:

    cd hyparse_fork
    mkdir lib
    cd lib

    # Download Eigen headers
    wget http://bitbucket.org/eigen/eigen/get/3.2.9.tar.bz2
    tar xvfj 3.2.9.tar.bz2
    cp -r eigen-eigen-dc6cfdf9bcec/Eigen .

    # Download boost headers
    wget https://sourceforge.net/projects/boost/files/boost/1.61.0/boost_1_61_0.tar.bz2
    tar xvfj boost_1_61_0.tar.bz2
    cp -r boost_1_61_0/boost .    
    
    

See the scripts in scripts/expe/ to run the parser. A typical training is done with:
        
    ./path_to/hyparse_fork/build/nnt -i ${iterations} -t ${templates} -m ${modelname} -l ${lr} -d ${dc} -H ${hiddensize} -a -f 2 -K ${dimensions} -L ${embeddings} ${train} ${dev}

Prediction is done then with:
        
    ./path_to/hyparse_fork/build/nnp -I ${test} -O ${preds} -m ${modelname}

## DATA 

* *data/*: only contains the list of files used to create the train/dev/test sets for each language, in order to replicate the full experiments (one file name per line, without extension)
* *data-sample/en/*: contains data from the RST DT that can be used to test the parser, and the data used to build the final files
    * dev/train*.tbk*: features files used by the discourse parser
    * dev/test/train*.raw*: raw files, for evaluating the discourse parser
    * *constituent_format/*: RST trees in *dmrg* format with the text of the EDUs directly included (words are separated by __)
    * *dependency_format/*: RST trees in *dconll* format, i.e. EDU index, EDU text, position of the head (nucleus of the relation or left most element if multi-nuclei relation), name of the relation to the head. These files are used by the parser to lexicalize the trees.
    * *parse-ud/*: dependency parses obtained with UD pipe for the files included in the sample
    * *parse-ud_edu*: dependency parses augmented with the index of the doc and the EDU for each token (used to build some features)
    * *dmrg_files*: original *dmrg* files as produced by the code in preprocess_rst/, used to produce the constituent and dependency format
    
### PRE-PROCESS DATA

*scripts/preprocess_data/* contains the code used to produce the files used by the parser.
Note that first, the data have to be read with the code in ../preprocess_rst/, in order to produce the *.dmrg* and *.edus* files.

* preprocess_data.sh: general script, for now only deal with the final conversion to *.tbk* and *.raw* files used by the parser **TODO** finish it!
        
    bash path_to/preprocess_data.sh path_to_inpath/data-sample/en/ path_to_outpath/data-sample/en/
        
* convert2dmrg-edus.py: add EDU text to the dmrg files, and output the train/dev/test files (i.e. constituent_format/)
* convert2dconll.py: find the head of each relation, and output a *dconll* file for each train/dev/test set (i.e. dependency_format)
* addEdu2conll.py: add the index of each EDU to the files containing the dependency parse of each document (*.conll* files), thus match the tokens in the parse with those in the EDUs (i.e. parse-ud_edu/)
* addTranslation.py: add a column with the translation of the token, or NONE. **TODO** add to data_sample
* process_pdtb.py: build the feature files used by the discourse parser

**TODO**: add code to parse with UD? (just done using models available on the website, parsing the raw text of each document)

## EVALUATION

* scripts/eval_parser.py: output scores (arguments: --preds pred_file --golds gold_file, both in bracketed/dmrg format). 

**TODO**: corresponds to the version of Ji and Eisenstein, but contains an error: scores are computed using a macro-average over the document. Change the scorer, and add the new scores to the README.

## RUN EXPE

* scripts/expe/run_expe.sh **TODO** finish script

## EXPE FILES

* expe/dim/: dimension for each features, required by the discourse parser
* expe/tpls/: templates used by the discourse parser, describing where to extract features
* expe/preds/: prediction files for the best experiments, for now only on the RST DT (**TODO** add the pred files for the other languages)


## Contact

Feel free to send me an email: chloe.braud@gmail.com