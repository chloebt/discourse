"""
A neural network based discourse tagger
:author:chloe braud
Required dynet (conda env dynet)
"""
from __future__ import print_function

import argparse, random, time, sys, os, pickle, gzip
import dynet as dy
import numpy as np

from collections import Counter, defaultdict
from itertools import count


'''
Original format: word\ttag
To use other features, for now one file per feature ie
- main      word\tag
- others eg pos\ttag (tag is not required)

TODO: one file with all the features, stg like word\ttag\tpos\tetc
'''


def main( ):
    parser = argparse.ArgumentParser(
            description='Train a discourse segmenter, eval on dev set.')
    parser.add_argument('--train', dest='train', nargs='+', action='store',
            help='Train files [default: word and pos]')
    parser.add_argument('--dev', dest='dev', nargs='+', action='store',
            default=['discoursecph/improvediseg/data/en/sentence/word/dev/','discoursecph/improvediseg/data/en/sentence/posgptb/dev/'],
            help='Dev files [default: word and pos]')
    parser.add_argument('--test', dest='test', nargs='+', action='store',
            default=['discoursecph/improvediseg/data/en/sentence/word/test/','discoursecph/improvediseg/data/en/sentence/posgptb/test/'],
            help='Test files [default: word and pos]')
    parser.add_argument('--in_dim', dest='in_dim', nargs='+', action='store',
            default=[300, 64],
            help='Input dimensions [default: 300 for feat0, 64 for feat1]')
    parser.add_argument('--h_dim', dest='h_dim', action='store',
            default=200,
            help='hidden dimension [default: 200]')
    parser.add_argument('--h_layers', dest='h_layers', action='store',
            default=1,
            help='number of hidden layers [default: 1]')
    parser.add_argument('--max_iter', dest='max_iter', action='store',
            default=10,
            help='Max number of iterations [default: 10]')
    parser.add_argument('--trainer', dest='trainer', action='store',
            choices = ['ssgd', 'adam'],
            default='adam',
            help='Trainer')
    parser.add_argument('--outemb', dest='outemb', action='store',
            default=None,
            help='If not None, write the final word embeddings in the provided file [default: None]')
    parser.add_argument('--model', dest='model', action='store',
            default=None,
            help='Output file to save the model [default: None]')

    parser.add_argument('--embed', dest='embed', action='store',
            default=None,
            #default='/Users/chloebraud/data/corpora/lexiques/embeddings/glove.6B/glove.6B.300d.txt',
            help='Embedding file [default: None]')

    parser.add_argument('--dynet_mem', dest='dynet_mem', action='store',
            help='dynet memory')

    parser.add_argument('--outpath', dest='outpath', action='store',
            default='projects/navi/neuralnet/data/preds_models/',
            help='Output directory for saving preds and models')
    args = parser.parse_args()

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    print( "\n"+" ".join( [
        "MAX-ITER:"+str( args.max_iter ),
        "INDIM:"+','.join([str(i) for i in args.in_dim]),
        "HDIM:"+str(args.h_dim),
        "HLAYERS:"+str(args.h_layers)
        ] ), file=sys.stderr )


#     print( args.model, args.train )
#     if args.model and not args.train:
#         print( "LOADING MODEL" )
#         model = load_model (args.model )


    # - Check same number of train and dev files
    if len( args.train) != len( args.dev ):
        sys.exit( "Give one dev file per train file" )
    # - Check same number of doc for each train set
    n_feat_files = len( [_d for _d in os.listdir( args.train[0] ) if not _d.startswith('.')] )
    for tr in args.train:
        temp = len( [_d for _d in os.listdir( tr ) if not _d.startswith('.')] )
        if temp != n_feat_files:
            sys.exit( "Not the same number of files in the training dir "+str(n_feat_files)+" "+str(temp) )


    embeds = None
    if args.embed:
        embeds = load_embeddings( args.embed )

    n_features = len( args.train )

    in_dims = [int(i) for i in args.in_dim][:n_features] #1 indim per feature
    features = [x for x in range( len(in_dims ) )] # name the features with indices
    trains, devs, tests = read_files( args.train ), read_files(args.dev), read_files(args.test) # one train/dev per feat

    feat2vocab, vt, ntags = init_vocab( trains, features ) #init dict for vocab of the features

    mytagger = Tagger(  in_dims,            # input dim for each feature
                        int(args.h_dim),    # hidden dim
                        int(args.h_layers), # numb of hidden layers
                        features,           # features names/indices
                        feat2vocab,         # feat -> (vocab, size vocab) TODO make a list rather than a dict
                        vt,                 # label vocab
                        ntags,              # numb of labels
                        args.trainer,       # trainer
                        args.model,          # model to load/dump model
                        embeds              # pre trained embeddings, or None
                        )
    mytagger.train( trains, devs, tests, int(args.max_iter), args.outpath ) #Give devs to retrieve score for each ite


    #TODO add test option

def load_model( inpath ):
    model = dy.Model()
    (fwdRNN, bwdRNN, pH, pO, lookups) = model.load( inpath )
    feat2vocab = pickle.load( os.path.join( inpath, "vocab" ))
    return model


def load_embeddings( embed, sep=" " ):
    vocab = {}
    vectors = []
    with open(embed) as f:
        f.readline()
        for i, line in enumerate(f):
            fields = line.strip().split(sep)
            vocab[fields[0]] = i
            vectors.append(list(map(float, fields[1:])))
    print( "Embeddings, vocab:", len(vectors), len( vocab ), 'Dim:', len( vectors[0] ) )


class Tagger:
    def __init__( self, in_dims, h_dim, h_layers, features, feat2vocab, vt, ntags, trainer, modelpath, embeds ):
        #if model: # dump

        self.model = dy.Model() # Model object
        self.modelpath = modelpath # Model file
        self.trainer = define_trainer( trainer, self.model ) # Trainer algo
        self.pre_embeds = embeds # pre trained embeddings or None if rand init (not used for now)
        self.in_dim = sum( in_dims ) # in dim: sum over the dim of concatenated vectors
        self.h_layers = h_layers
        self.h_dim = h_dim # hidden layers dimension
        self.vt, self.ntags = vt, ntags # label vocab and size
        self.feat2vocab = feat2vocab # feat -> (vocab, size vocab)
        self.features = features
        self.nfeatures = len( self.feat2vocab )
        # one LOOKUP object per feature
        self.feat2lookup = {}
        self.lookups = []
        for i,f in enumerate( self.feat2vocab ):
            self.feat2lookup[f] = self.model.add_lookup_parameters(( self.feat2vocab[f][1], in_dims[i]))
            self.lookups.append( self.feat2lookup[f] )
        # Forward and Backward (numb of layers, input dim, hidden dim, model)
        self.fwdRNN = dy.LSTMBuilder(self.h_layers, self.in_dim, self.h_dim, self.model)
        self.bwdRNN = dy.LSTMBuilder(self.h_layers, self.in_dim, self.h_dim, self.model)
        # Parameter from hidden layers to output MLP. Here, use MLP to predict the labels
        self.pH = self.model.add_parameters((32, self.h_dim*2)) # ?? 32
        # Parameter from output MLP to predicted labels
        self.pO = self.model.add_parameters((ntags, 32))


    def train( self, trains, dev, test, niter, outpath ):
        rseed = random.seed( 1234 )
        num_tagged = cum_loss = 0
        for ITER in xrange(1, niter+1):
            print( "\n"+"ITER: "+str(ITER), file=sys.stderr )
            # Shuffle the training sets
            combined = list( zip( *trains ) )
            random.shuffle(combined)
            trains = zip(*combined)
            train = trains[0] #the other (optional) train files are for additional features

            for j,d in enumerate( train, 1 ): # could be document or sentence

                if j>0 and j % 1000 == 0: #printstatus
                    self.trainer.status()
                    print( cum_loss / num_tagged, file=sys.stderr )
                    cum_loss = num_tagged = 0
                # train on doc/sent
                words = [w for w,t in d]
                golds = [t for w,t in d]
                if len( words ) == 0:
                    sys.exit( "no feats" )
                # Add features
                feat2train = {} # feat -> list of features for this doc ie pos, supertags ...
                if self.nfeatures > 1:
                    for i,f in enumerate( self.features[1:] ):
                        feat2train[f] = [p for p,_ in trains[i+1][j-1]] # TODO put the words also, only one structure for all
                loss_exp =  self.sent_loss(words, golds, feat2train)
                cum_loss += loss_exp.scalar_value()
                num_tagged += len(golds)
                loss_exp.backward()
                self.trainer.update()

            if dev:
                preds = self.predict( dev )
                # write the preds for each ite to avoid retraining
                write_pred( preds, ITER, outpath, dset='dev' )
                if self.modelpath:
                    self.save_model( outpath, ITER )

            if test:
                preds = self.predict( test )
                # write the preds for each ite to avoid retraining
                write_pred( preds, ITER, outpath, dset='test' )

    def save_model( self, outpath, ITER ):
        tosave = [self.fwdRNN, self.bwdRNN, self.pH, self.pO]
        tosave.extend( self.lookups )
        self.model.save( os.path.join( outpath, "model_it"+str(ITER) ),
                tosave )
        pickle.dump( self.feat2vocab, open( os.path.join( outpath, "feat2vocab_it"+str(ITER) ), 'w' ), pickle.HIGHEST_PROTOCOL )

    def predict( self, devs ):
        dev = devs[0]
        preds = []
        for j,doc in enumerate( dev ):
            doc_preds = []
            words = [w for w,t in doc]
            golds = [t for w,t in doc]
            feat2train = {} # feat -> list of features for this doc ie pos, supertags ...
            if self.nfeatures > 1:
                for i,f in enumerate( self.features[1:] ):
                    feat2train[f] = [p for p,_ in devs[i+1][j]]
            tags = [t for w,t in self.tag_sent(words, feat2train)]
            for w,go,gu in zip(words,golds,tags):
                doc_preds.append( tuple([w,go,gu]) )
            preds.append( doc_preds )
        return preds

    def tag_sent(self, words, feat2train):
        vecs = self.build_tagging_graph(words, feat2train)
        vecs = [dy.softmax(v) for v in vecs]
        probs = [v.npvalue() for v in vecs]
        tags = []
        for prb in probs:
            tag = np.argmax(prb)
            tags.append(self.vt.i2w[tag])
        return zip(words, tags)

    def sent_loss(self, words, tags, feat2train):
        vecs = self.build_tagging_graph(words, feat2train)
        losses = []
        for v,t in zip(vecs,tags):
            tid = self.vt.w2i[t]
            loss = dy.pickneglogsoftmax(v, tid)
            losses.append(loss)
        return dy.esum(losses)

    def build_tagging_graph(self, words, feat2train):
        dy.renew_cg()
        # initialize the RNNs
        f_init = self.fwdRNN.initial_state()
        b_init = self.bwdRNN.initial_state()
        feats = [words] #TODO add earlier, rm words everywhere
        for f in feat2train:
            feats.append( feat2train[f] )
        wembs = [self.word_rep( f ) for f in zip(*feats)]

        # Propagation
        fws = f_init.transduce(wembs)
        bws = b_init.transduce(reversed(wembs)) # biLSTM states
        # Concatenate biLSTM states
        bi = [dy.concatenate([f,b]) for f,b in zip(fws, reversed(bws))]

        # Add noise, should be only for the output layer?
#         if train and self.noise_sigma > 0.0:
#             concat_layer = [pycnn.noise(fe,self.noise_sigma) for fe in concat_layer]

        # MLPs
        H = dy.parameter(self.pH)
        O = dy.parameter(self.pO)
        outs = [O*(dy.tanh(H * x)) for x in bi]
        return outs

    def word_rep(self, features ):
        for i,f in enumerate( features ):
            if i == 0: #initiate final vector
                if f in self.feat2vocab[i][0].w2i:
                    f_index = self.feat2vocab[i][0].w2i[f]
                else:
                    f_index = self.feat2vocab[i][0].w2i['_UNK_']
                vect = self.feat2lookup[i][f_index]
            else:
                if f in self.feat2vocab[i][0].w2i:
                    f_index = self.feat2vocab[i][0].w2i[f]
                else:
                    f_index = self.feat2vocab[i][0].w2i['_UNK_']
                vect = dy.concatenate( [vect, self.feat2lookup[i][f_index]] )
        return vect


def define_trainer( trainer, model ):
    if trainer == "adam":
        return dy.AdamTrainer( model )
    elif trainer == "ssgd":
        return dy.SimpleSGDTrainer( model )
    elif trainer == "msgd":
        return dy.MomentumSGDTrainer( model )
    elif trainer == "adag":
        return dy.AdagradTrainer( model )
    elif trainer == "adad":
        return dy.AdadeltaTrainer( model )
    else:
        sys.exit( "Unknown trainer "+trainer )



def init_vocab( trains, features ):
    '''
    Return
    - a dict: feat number -> (vocab, size vocab)
    - the tag vocabulary
    - the number of tags
    '''
    feat2vocab = {}
    for i,f in enumerate( features ):
        if i == 0: # compute tag vocab only once, but it doesn t matter that 0 = words
            vw, nwords, vt, ntags = _init_vocab( trains[0], unk="_UNK_" )
            feat2vocab[f] = tuple([vw, nwords])
        else:
            vp, npos, _, _ = _init_vocab( trains[1], unk="_UNK_", tag=False )
            feat2vocab[f] = tuple([vp, npos]) # pos or stg else
    return feat2vocab, vt, ntags


def _init_vocab( train, unk, tag=True ):
    words=[]
    tags=[]
    vt, ntags = None, 0
    wc=Counter()
    for doc in train:
        for w,p in doc:
            words.append(w)
            if tag:
                tags.append(p)
            wc[w]+=1
    words.append(unk)
    vw = Vocab.from_corpus([words])
    UNK = vw.w2i[unk]
    if tag:
        vt = Vocab.from_corpus([tags])
    nwords = vw.size()
    if tag:
        ntags  = vt.size()
    return vw, nwords, vt, ntags

def write_pred( preds, ite, outpath, dset='dev' ):
    ''' Write a file with the predicted tags '''
    o = open( os.path.join( outpath, "pred_"+dset+"_"+str(ite)+".txt" ), 'w'  )
    o.write( "Eval on Dev\n" ) #scorer needs the first line to be ignored
    for j,doc in enumerate( preds ):
        if j != 0: # Retrieve document/sentence sep
            o.write( "\n" )
        for w,go,gu in doc:
            o.write( "\t".join( [w, go, gu] )+"\n" )
    o.close()


class Vocab:
    def __init__(self, w2i=None):
        if w2i is None:
            w2i = defaultdict(count(0).next)
        self.w2i = dict(w2i)
        self.i2w = {i:w for w,i in w2i.iteritems()}
    @classmethod
    def from_corpus(cls, corpus):
        w2i = defaultdict(count(0).next)
        for sent in corpus:
            [w2i[word] for word in sent]
        return Vocab(w2i)

    def size(self):
        return len(self.w2i.keys())


def read_files( files ):
    """
    Read a tagged file where each line is of the form "word1/tag1 word2/tag2 ..."
    Return a list of doc (doc or sentence) corresponding to a list of [(word1,tag1), (word2,tag2), ...]
    """
    sets = [] # 1 train per feature
    for fname in files:
        docs = []
        for f in [_f for _f in os.listdir( fname ) if not _f.startswith( '.')]:
            #print( f )
            doc = []
            with open( os.path.join( fname, f ) ) as fh:
                for line in fh:
                    line = line.strip()
                    if line != '': # last line
                        line = line.split()
                        tok_tag = tuple( [line[0].strip(), line[1].strip()] )
                        doc.append( tok_tag )
            docs.append( doc )
        print( " ".join( [ fname, "#doc", str(len( docs ) ) ] ), file=sys.stderr )
        sets.append( docs )
    return sets


if __name__ == '__main__':
    main()
