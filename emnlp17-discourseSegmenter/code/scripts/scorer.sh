#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e

PRED=$1
MAXITER=$2
HLAY=$3
HDIM=$4
SRC=$5

SIGMA=0

echo
echo Scores with the sentences with no EDUs removed
 
for ITER in $(seq 1 ${MAXITER})
do
#     echo ${ITER} [ -e "${PRED}/pred_dev_${ITER}.txt" ]
    if [ -e "${PRED}/pred_dev_${ITER}.txt" ]
    then 
        PARAMS=${ITER}"&"${HLAY}"&"${HDIM}"&"${SIGMA}
    python ${SRC}/code/segmenter_scorer.py --preds ${PRED}/pred_dev_${ITER}.txt -m ${PARAMS}
fi
done

echo
echo Scores keeping all the sentences
for ITER in $(seq 1 ${MAXITER})
do
#     echo ${ITER} [ -e "${PRED}/pred_dev_${ITER}.txt" ]
    if [ -e "${PRED}/pred_dev_${ITER}.txt" ]
    then 
        PARAMS=${ITER}"&"${HLAY}"&"${HDIM}"&"${SIGMA}
    python ${SRC}/code/segmenter_scorer.py --preds ${PRED}/pred_dev_${ITER}.txt -m ${PARAMS} -x all
fi
done



 
