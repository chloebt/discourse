#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
#set -e


SRC=$1
OUT=$2
mkdir -p ${OUT}

CODE=${SRC}/code/
DATA=${SRC}/data/en/

WD=${DATA}word/
POSU=${DATA}posud/

MAXITER=20
HLAY=1


for INDIMw in 300 #50 100 200 
do
    for INDIMp in 64 #32 
    do
        for HDIM in 200 #400
        do

        PRED=${OUT}preds_doc-words+posud-en-id${INDIMw}_${INDIMp}-hd${HDIM}-hl${HLAY}/
        mkdir -p ${PRED}

        echo DOC WORDS+POSUD ITER ${MAXITER} INDIM ${INDIMw} ${INDIMp} HDIM ${HDIM} HLAYERS ${HLAY}

        INDIM=(${INDIMw} ${INDIMp})
        python ${CODE}segmenter.py --train ${WD}train/ ${POSU}train/ --dev ${WD}dev/ ${POSU}dev/ --test ${WD}test/ ${POSU}test/ --in_dim ${INDIM[*]} --h_dim ${HDIM} --h_layers ${HLAY} --max_iter ${MAXITER} --outpath ${PRED}

        done
    done
done



