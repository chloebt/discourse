#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import argparse, os, sys, shutil, subprocess, numpy, re
#from convert_tree_utils import *
#from nltk import Tree
import numpy as np

'''

Discourse segmentation is viewed as a binary task: insert a boundary or not
(In general, it is insert a boundary AFTER the word w, here I did BEFORE (ie BI encoding))

Evaluation corresponds to computing the ability to correctly identify the boundaries
   (ie. we thus just give scores for the B label) See Soricut and Marcu 2003 and following
   (vs counting B and I, as done in Marcu 2000)

'''



def main( ):
    parser = argparse.ArgumentParser(
            description='Scores for discourse segmentation.')
    parser.add_argument('-p', '--preds',
            dest='preds',
            action='store',
            help='Predictions')
    parser.add_argument('-a', '--algo',
            dest='algo',
            action='store',
            default="lstm",
            help='Algo')
    parser.add_argument('-x', '--mode',
            dest='mode',
            action='store',
            default="all",
            choices=['rm', 'all'],
            help='Mode: remove the sentences with only 1 B (rm) or keep all the sentences (all)')
    parser.add_argument('-f', '--feat',
            dest='feat',
            action='store',
            default="pos",
            help='Features  (pos)')
    parser.add_argument('-m', '--params',
            dest='params',
            action='store',
            default=None,
            help='Parameters of the expe: the script output params\ scores per input file, of Nonde, try to retrieve parameters values from the name of the pred file')
    parser.set_defaults(nuclearity=False)
    args = parser.parse_args()


    if os.path.isfile( args.preds ):
#     print( "PRINT SCORES", file=sys.stderr )
        score( args.preds, args.algo, args.params, args.mode )
    elif os.path.isdir( args.preds ):
        score_dir( args.preds, args.algo, args.params, args.mode, feat=args.feat )



def score_dir( dir_preds, algo, aparams, mode, feat='pos' ):
    #res = {}
    res_dev = {}
    res_test = {}
    for sdir in os.listdir( dir_preds ):
        if '-'+feat+'-' in sdir:
            #print( "\\\\midrule" )
            hlay = sdir[-1]
            hdim = sdir.split('-')[-2][2:]
            indim = sdir.split('-')[-3][2:]
            if not hlay in res_dev:
                res_dev[hlay] = {}
                res_test[hlay] = {}
            if not hdim in res_dev[hlay]:
                res_dev[hlay][hdim] = {}
                res_test[hlay][hdim] = {}
            if not indim in res_dev[hlay][hdim]:
                res_dev[hlay][hdim][indim] = {}
                res_test[hlay][hdim][indim] = {}

            _preds = os.path.join( dir_preds, sdir )



            for p in os.listdir( _preds ):
                if 'pred' in p and 'dev' in p:
                    pred = os.path.join( dir_preds, sdir, p )
                    doc2preds, new_doc2preds = read_preds( pred, algo ) # with all sentences, with no B sentences removed
#                     doc2preds = new_doc2preds

                    #${ITER}"&"${HLAY}"&"${HDIM}"&"${SIGMA}
                    iterr = p.split('.')[0].split('_')[-1]
                    if not iterr in res_dev[hlay][hdim][indim]:
                        res_dev[hlay][hdim][indim][iterr] = []
                    aparams = iterr+'&'+hlay+'&'+hdim+'&'+indim
                    if mode == "all":
                        p,r,f = _score( doc2preds, aparams  )
                        res_dev[hlay][hdim][indim][iterr] = [p,r,f]
                elif 'pred' in p and 'test' in p:
                    pred = os.path.join( dir_preds, sdir, p )
                    doc2preds, new_doc2preds = read_preds( pred, algo ) # with all sentences, with no B sentences removed
#                     doc2preds = new_doc2preds


                    #${ITER}"&"${HLAY}"&"${HDIM}"&"${SIGMA}
                    iterr = p.split('.')[0].split('_')[-1]
                    if not iterr in res_test[hlay][hdim][indim]:
                        res_test[hlay][hdim][indim][iterr] = []
                    aparams = iterr+'&'+hlay+'&'+hdim+'&'+indim
                    if mode == "all":
                        p,r,f = _score( doc2preds, aparams  )
                        res_test[hlay][hdim][indim][iterr] = [p,r,f]

    print( "DEV RES" )
    print_res( res_dev )

    print( "\n\nTEST RES" )
    print_res( res_test )

def print_res( res ):
    best = []
    for hlay in sorted( res.keys(), lambda x,y:cmp(int(x),int(y)) ):
        print( '\\midrule' )
        for hdim in sorted( res[hlay],  lambda x,y:cmp(int(x),int(y)) ):
            print( '\\midrule' )
            for indim in sorted( res[hlay][hdim],  lambda x,y:cmp(int(x.split('_')[0]),int(y.split('_')[0])) ):
                print( '\\midrule' )
                bs, i = 0.,0
                for iterr in sorted( res[hlay][hdim][indim],  lambda x,y:cmp(int(x),int(y)) ):
                    p,r,f = res[hlay][hdim][indim][iterr]
                    print( hlay+' & '+hdim+' & '+indim.replace( '_', ' ')+' & '+iterr+' & '+str(round(p*100,3))+' & '+str(round(r*100,3))+' & '+str(round(f*100,3))+'\\\\' )
                    if f > bs:
                        bs = f
                        i = iterr
                best.append( [hlay, hdim, indim, i, round(bs*100,3)] )

    print( "\nBEST\n" )
    print( "\t&\t".join( ['HLAY', 'HDIM', 'INDIM', 'ITER', 'F1'] )+'\\\\' )
    for b in best:
        print( "\t&\t".join( [str(v).replace( '_', ' ') for v in b] )+'\\\\' )


def score( pred, algo, aparams, mode ):
    #for pred in preds:
    doc2preds, new_doc2preds = read_preds( pred, algo ) # with all sentences, with no B sentences removed
    if mode == "all":
        print( _score( doc2preds, aparams  ))
    elif mode == "rm":
        print( _score( new_doc2preds, aparams ) )


def _score( doc2preds, aparams ):
    c = correct( doc2preds )
    m = predicted( doc2preds )
    h = gold( doc2preds )
    if c == 0:
        precision = 0.
        recall = 0.
        fscore = 0
    else:
        precision = c/m
        recall = c/h
        fscore = (2*c)/(h+m)

    if aparams == None:
        params=set_params( pred )
    else:
        params = aparams
    return precision, recall, fscore
#     return print_out( params, precision, recall, fscore )

    #pprint( params, precision, recall, fscore )
    #print_avg( doc2preds )

def set_params( pred ):
    return os.path.basename( pred )#.split('.')

def read_preds( preds, algo ):
    if algo == "lstm":
        doc2preds, new_doc2preds = read_preds_lstm( preds )
    elif algo == "rung" or algo == "rungsted":
        doc2preds = read_preds_rung( preds )
    else:
        sys.exit( "Unknown algo:"+algo )
    return doc2preds, new_doc2preds

def read_preds_lstm( preds ):
    doc2preds = {} # Doc -> [ (gold, pred), ...]
    id_doc, count_token = 0,0
    with open( preds ) as myfile:
        for _line in myfile.readlines()[1:]:#1 non used line
            _line = _line.strip()
            if _line == "":
                id_doc += 1
                continue
            token, gold, pred = _line.split('\t')
            if id_doc in doc2preds:
                doc2preds[id_doc].append( [gold, pred] )
            else:
                doc2preds[id_doc] = [ [gold, pred] ]
            count_token += 1
#     print( "#Samples: ",count_token,
#             "(", len( [ g for doc in doc2preds for (g,p) in doc2preds[doc]] ), ")", file=sys.stderr )
#     print( "#Doc:", id_doc, "(", len(doc2preds), ")", file=sys.stderr )

    # Remove doc/sent containing only one (gold) B (ie the beg of the sent/doc)
    removed = 0
    new_doc2preds = {}
    for d in doc2preds:
        temp = [ g for (g,_) in doc2preds[d] if g=='B' ]
        if len( temp ) != 1:
            new_doc2preds[d] = doc2preds[d]
        else:
            removed += 1
#     print( "Removed", removed, file=sys.stderr )

    return doc2preds, new_doc2preds


def read_preds_rung( preds ):
    doc2preds = {} # Doc -> [ (gold, pred), ...]
    id_doc, count_token = 0,0
    with open( preds ) as myfile:
        for _line in myfile.readlines():
            _line = _line.strip()
            if _line == "":
                id_doc += 1
                continue
            id_token, gold, pred = _line.split('\t')
            gold = gold.replace( 'b\'', '').replace( '\'', '')
            pred = pred.replace( 'b\'', '').replace( '\'', '')
            if id_doc in doc2preds:
                doc2preds[id_doc].append( [gold, pred] )
            else:
                doc2preds[id_doc] = [ [gold, pred] ]
            count_token += 1
#     print( "#Samples: ",count_token,
#             "(", len( [ g for doc in doc2preds for (g,p) in doc2preds[doc]] ), ")", file=sys.stderr )
#     print( "#Doc:", id_doc, "(", len(doc2preds), ")", file=sys.stderr )
    return doc2preds




def print_out( params, precision, recall, fscore ):
    '''
    Simple print to be easily read in a final file
    Params Prec Rec F
    '''
#     print( ' '.join( [params, str(round( precision*100, 2 ) ),
#         str( round( recall*100, 2 ) ),
#         str( round( fscore*100, 2 ) ) ] )
#         )
    # Temp print, params = only iter (+' & & & & ')
#     print( params+' & '+' & '.join( [str(round( precision*100, 2 ) ),
#         str( round( recall*100, 2 ) ),
#         str( round( fscore*100, 2 ) ) ] )+'\\\\'
#         )

    return ''.join( [params+' & '+' & '.join( [str(round( precision*100, 2 ) ),
        str( round( recall*100, 2 ) ),
        str( round( fscore*100, 2 ) ) ] )+'\\\\' ] )

def pprint( params, precision, recall, fscore ):
    '''
    More easy to read + adding other info (eg nb of correct .)
    '''
    print( "\nGlobal scores:", file=sys.stderr )
    print( "\t&\t".join( ["P", "R", "F"] )+"\\\\", file=sys.stderr )
    print( "\t&\t".join( [str( round( precision*100, 2 ) ),
        str( round( recall*100, 2 ) ),
        str( round( fscore*100, 2 ) ) ] )+"\\\\", file=sys.stderr )
    print( "\nB gold:", h, " ; B predicted", m, " ; Correct", c, file=sys.stderr )


def print_avg( doc2preds ):
    '''
    Print avg scores over documents
    '''
    print( "\nAvg over documents:", file=sys.stderr )
    avg_p, avg_r, avg_f = [], [], []
    for doc in doc2preds:
        c = _correct( doc2preds[doc] )
        m = _predicted( doc2preds[doc] )
        h = _gold( doc2preds[doc] )
        if m == 0:
            avg_p.append( 0 )
        else:
            avg_p.append( c/m )
        if h == 0:#happen if a doc (ie a sentence) is an EDU, no intra boundaries
            avg_r.append( 0 )
        else:
            avg_r.append( c/h )
        if h+m == 0:
            avg_f.append( 0 )
        else:
            avg_f.append( (2*c)/(h+m) )
    print( "\t&\t".join( ["macroP", "macroR", "macroF"] )+"\\\\", file=sys.stderr )
    print( "\t&\t".join( [str( round( np.mean( avg_p )*100, 2 ) ),
        str( round( np.mean( avg_r )*100, 2 ) ),
        str( round( np.mean( avg_f )*100, 2 ) ) ] )+"\\\\", file=sys.stderr )



# ----
def correct( doc2preds ):
    # Count the number of boundaries (B) correctely predicted
    correct = 0.
    for doc in doc2preds:
        correct += _correct( doc2preds[doc] )
    return correct

# Ignore the first tag
def _correct( doc ):
    correct = 0.
    for (g,p) in doc[1:]:
        if g == 'B' and g == p:
            correct += 1.
#     if correct != 0:
#         correct -= 1 # 1 less per document (the first one does not need to be predicted)
    return correct



# ----
def predicted( doc2preds ):
    # Count the number of predicted boundaries (B)
    predicted = 0.
    for doc in doc2preds:
        predicted += _predicted( doc2preds[doc] )
    return predicted

def _predicted( doc ):
    predicted = 0.
    for (g,p) in doc[1:]:
        if p == 'B':
            predicted += 1.
#     if predicted != 0:
#         predicted -= 1
    return predicted


# ----
def gold( doc2preds ):
    # Count the number of gold boundaries (B)
    gold = 0.
    for doc in doc2preds:
        gold += _gold( doc2preds[doc] )
    return gold

def _gold( doc ):
    gold = 0.
    for (g,p) in doc:
        if g == 'B':
            gold += 1.
    gold -= 1
    return gold


if __name__ == '__main__':
    main()
