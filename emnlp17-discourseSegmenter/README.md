# README #

Code for a RST discourse segmenter based on UD parses. 

The segmenter can be used at the sentence level or the document level (one file per sentence or document). The data samples made available are documents.

A description of the system can be found in: Does syntax help discourse segmentation? Not so much, Chloé Braud, Ophélie Lacroix and Anders Søgaard, In Proceedings of EMNLP 2017, http://aclweb.org/anthology/D17-1257.

### Data ###

One file per feature (e.g. words, POS etc...), format: feature\tlabel, where label is B or I.

Data samples available in data/ (English, data from the RST DT), document-level:
* No sentence segmentation
* word/ and posud/: no gold tokenisation, POS predicted by UDPipe
* TODO: add stag-*/ supertags extracted from udpipe parses (hlab = head label, hdir = head direction, clab = children labels, cdir = children directions)
* TODO other languages/domains

### Code ###

* Segmenter:
    * Implemented in dynet: use Python2 and install dynet http://dynet.readthedocs.io/en/latest/python.html
    * Train and eval on dev and test the segmenter:
    ```
    python code/segmenter.py --train train1/ train2/ --dev dev1/ dev2/ --test test1/ test2/ --in_dim INDIM --h_dim HDIM --max_iter MAXITER --outpath PRED_DIR
    ```
    For example, using word and UD postags:
    ```
    python code/segmenter.py --train data/en/word/train/ data/en/posud/train/ --dev data/en/word/dev/ data/en/posud/dev/ --test data/en/word/test/ data/en/posud/test/ --in_dim INDIM --h_dim HDIM --max_iter MAXITER --outpath PRED_DIR
    ```
    * Running all experiments (varying the hyper-parameters, see the script), SRC contains the code and the data (e.g. emnlp17-discourseSegmenter/ contains code/ and data/en/), OUT will contain the different PRED_DIR/
    ```
    bash code/run.sh SRC OUT
    ```

* Scorer: 
    * Rec Prec and F1 over the B labels
    * ignoring the B beginning a sentence/document
    * ignoring the sentences containing no gold B or taking all the sentences into account
    * Run segmenter_scorer.py on all the pred files of a directory: output the scores as latex tabular lines, SRC contains the code (e.g. emnlp17-discourseSegmenter/)
    ```
    bash code/scripts/scorer.sh path_to_preds/ MAXITER HLAY HDIM SRC >path_to_file_scores
    ```