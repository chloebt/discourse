#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import argparse, os, sys, shutil, numpy, re
from nltk import Tree


def main( ):
    parser = argparse.ArgumentParser(
            description='Scores for constituent discourse parsing from a tree\
                    transformed into a sequence. Transform the predicted \
                    sequences back to a tree and evaluate parsing against\
                    the gold tree.')

    parser.add_argument('-t', '--test',
            dest='test',
            action='store',
            help='Test files, ie gold trees in bracketed format (.mrg)')
    parser.add_argument('-c', '--cpred',
            dest='cpred',
            action='store',
            help='Prediction for constituent form.')
    args = parser.parse_args()

    if not args.cpred:
        sys.exit( "Please provide a prediction file (--cpred)")
    if not args.test:
        sys.exit( "Please provide a directory containing the gold trees (--test).")

    score_constituent( args.cpred )


def score_constituent( cpred ):
    print( "Retrieve gold and pred trees..", file=sys.stderr )
    doc2GoldPred = read_preds( cpred ) # Retrieve trees from pred files
    doc2GoldPredTrees = getTrees( doc2GoldPred ) # Build trees from str trees, ev with correction
    print( "Compute scores..", file=sys.stderr )
    met = Metrics(levels=['span','nuclearity','relation'])
    for doc in doc2GoldPredTrees:
        goldTree, predTree = doc2GoldPredTrees[doc]
        bracketsPred = bracketForm( predTree )
        bracketsGold = bracketForm( goldTree )
        met.eval(bracketsGold, bracketsPred)
    met.report()


def read_preds( predFile ):
    nb_document = 0
    doc2GoldPred = {} # Doc -> (id_edu, text_edu, gold_fragment, pred_fragment)
    nb_edu = 1
    with open( predFile ) as myfile:
        for _line in myfile.readlines()[1:]:
            _line = _line.strip()
            if _line == "":
                nb_document += 1
                nb_edu = 1
                continue
            edu, gold, pred = _line.split('\t')
            if nb_document in doc2GoldPred:
                doc2GoldPred[nb_document].append( [nb_edu, edu, gold, pred] )
            else:
                doc2GoldPred[nb_document] = [[nb_edu, edu, gold, pred]]
            nb_edu += 1
    print( "# Documents", len( doc2GoldPred ), file=sys.stderr )
    return doc2GoldPred


# ----------------------------------------------------------------------------------
# RETRIEVE (WELL-FORMED) TREES FROM PRED FILE
# ----------------------------------------------------------------------------------
def getTrees( doc2GoldPred ):
    doc2GoldPredTrees = {}
    countWellFormed = 0
    for doc in doc2GoldPred.keys():
        #print( "\nDoc", doc )
        strPredTree, strGoldTree = retrieveTreeStr( doc2GoldPred[doc] )
        # check if the trees are well formed, stop the program if the gold is not
        checkWellformedness( strGoldTree, type_="gold" )
        isWellFormed = checkWellformedness( strPredTree )
        if isWellFormed:
            predTree =  Tree.fromstring( strPredTree )
            # Need to go through corrections for non binary nodes
            predTree = lightCorrectTree( predTree )
            countWellFormed += 1
        else:
            # correct predTree: check for parenthesis, non binary and unary nodes
            predTree = correctTree( strPredTree )
        goldTree = Tree.fromstring( strGoldTree )
        doc2GoldPredTrees[doc] = ( goldTree, predTree )
    print( "Originaly well formed predicted trees:", countWellFormed, file=sys.stderr )
    return doc2GoldPredTrees

def retrieveTreeStr( strGoldPred ):
    '''
    Read the gold and predicted trees from the pred files
    ie each segment is associated with fragments such as (RelX(RelY( or RelZ)
    Retrieve the trees from the fragments, inserting the id of the EDUs as leaves
    '''
    pat = re.compile( '\\)' )
    predTree,goldTree = '',''
    for (edu_id, edu_text, gold, pred) in strGoldPred:
        if edu_id == 1 and not pred.startswith( '(' ):#the first EDU should be opening a relation
            pred = '('+pred[:-1]
        pmat = re.findall( pat, pred )
        # -- Pred Tree
        if len( pmat ) == 0: # opening fragment, add the EDU at the end ie (id)
            predTree += pred+'('+str(edu_id)+')'
        else: # clsing fragment, add the EDU at the begining, ignore the relation should be opened prev
            predTree += '('+str(edu_id)+')'+')'*len( pmat )
        # -- Gold Tree (idem)
        gmat = re.findall( pat, gold )
        if len( gmat ) == 0:
            goldTree += gold.strip()+'('+str(edu_id)+')'
        else:
            goldTree += '('+str(edu_id)+')'+')'*len(gmat)
    return predTree, goldTree

def getArgumentSpan( left, right ):
    ''' Span format for the arguments '''
    arg1_leaves = getLeaves( left )
    arg2_leaves = getLeaves( right )
    return _getArgumentSpan( arg1_leaves ), _getArgumentSpan( arg2_leaves )

def _getArgumentSpan( leaves ):
    if len( leaves ) == 1:
        return (leaves[0], leaves[0])
    else:
        return (leaves[0], leaves[-1])

def getLeaves( tree ):
    ''' Return the leaves (ie EDUs) covered by a relation '''
    leaves = set()
    children = [c for c in tree]
    if len( children ) == 0:
        return [int(tree.label())]
    while len(children) != 0:
        child = children.pop(0)

        if len([c for c in child]) == 0:
            leaves.add( child.label() )
        else:
            children.extend( [c for c in child] )
    return sorted( [int(l) for l in sorted( list(leaves), lambda x,y:cmp(x,y) )] )



# ----------------------------------------------------------------------------------
# CORRECT PRED TREES
# ----------------------------------------------------------------------------------
def correctTree( strTree, verbose=False ):
    '''
    Correct a (predicted) tree, ie:
    1) add closing parenthesis to match the opened ones
    2) deal with unary nodes
    3) deal with non binary nodes (more than 2 children)
    (check the final labels to deal with new labels created when dealing with 2) and 3))
    (+ final check to raise any uncovered error)
    '''
    # 1) Check number of parenthesis
    strTree = correctParenthesis( strTree )
    pTree = Tree.fromstring( strTree )
    if verbose:
        print( "Original tree" )
        Tree.draw( pTree )
    # 2) check that all internal nodes have 2 children
    pTree.chomsky_normal_form() # Check large families
    if verbose:
        print( "Deal with node with len( children) > 2" )
        Tree.draw( pTree )
    pTree = checkUnary( pTree ) # Check lonely kids.
    if verbose:
        print( "Deal with unary nodes" )
        Tree.draw( pTree )
    checkLabels( pTree ) # Modify artificial labels
    if verbose:
        print( "Modify labels" )
        Tree.draw( pTree )
    finalCheck( pTree ) # Check unary and non binary nodes
    if verbose:
        print( "Final check" )
        Tree.draw( pTree )
    return pTree

def lightCorrectTree( pTree, verbose=False ):
    '''
    Correct a (predicted) tree, ie:
    2) deal with unary nodes
    3) deal with non binary nodes (more than 2 children)
    (check the final labels to deal with new labels created when dealing with 2) and 3))
    (+ final check to raise any uncovered error)
    '''
    # 2) check that all internal nodes have 2 children
    pTree.chomsky_normal_form() # Check large families
    if verbose:
        print( "Deal with node with len( children) > 2" )
        Tree.draw( pTree )
    pTree = checkUnary( pTree ) # Check lonely kids.
    if verbose:
        print( "Deal with unary nodes" )
        Tree.draw( pTree )
    checkLabels( pTree ) # Modify artificial labels
    if verbose:
        print( "Modify labels" )
        Tree.draw( pTree )
    finalCheck( pTree ) # Check unary and non binary nodes
    if verbose:
        print( "Final check" )
        Tree.draw( pTree )
    return pTree


def correctParenthesis( strTree ):
    '''
    Add final parenthesis
    Remove internal closing parenthesis that end the tree too early
    '''
    opened = 0
    closed = 0
    tmp = ''
    for i,c in enumerate( strTree ):
        if c == "(":
            tmp += c
            opened += 1
        elif c == ')':
            closed += 1
            # Tree closed too early
            if opened == closed and i !=len( strTree ):
                tmp += ''
                closed -= 1
            else:
                tmp += c
        else:
            tmp += c
    if opened > closed:
        correctedStrTree = tmp+")"*(opened-closed)
    else:
        # TODO assume that all final caracters are parenthesis ... not really good
        correctedStrTree = tmp
        for i in range( closed-opened ):
            correctedStrTree = correctedStrTree[:-1]
    # check well formedness of the new tree
    isWellFormed = checkWellformedness( correctedStrTree )
    if not isWellFormed:
        #tryCorrect( strTree )
        print( strTree )
        print( "\n", correctedStrTree )
        sys.exit( "Still not well formed tree" )
    return correctedStrTree


def checkUnary(tree, collapsePOS = True, collapseRoot = True, joinChar = "+"):
    if collapseRoot == False and isinstance(tree, Tree) and len(tree) == 1:
        nodeList = [tree[0]]
    else:
        nodeList = [tree]
    # depth-first traversal of tree
    while nodeList != []:
        node = nodeList.pop()
        if isinstance(node,Tree):
            if len(node) == 1 and isinstance(node[0], Tree) and (collapsePOS == True or isinstance(node[0,0], Tree)):
                if isRoot( tree, node ):
                    # Replace the root by its child
                    tree = node[0]
                    nodeList = [tree]
                elif isPreterminal( node ):
                    # (RelX (A) (RelY (B) ) ) -> keep only relX
                    # replace the node by its child (ie a leave)
                    replaceNode( tree, node, node[0] )
                elif isPreEDU( node ):
                    # RelX node before an EDU that is not a leave, ie the EDU node has a child
                    # Modify the children of the node
                    edu = node[0]
                    eduChild = node[0][0]
                    newnode = Tree( node.label(), [Tree( edu.label(), [] ), eduChild] )
                    replaceNode( tree, node, newnode)
                    nodeList.append(newnode)
                else:
                    # General case ie RelY has one child which is also a RelZ
                    # (RelX (Stg) (RelY (RelZ (stg..) ) ) )
                    # As for the root, remove the relY node
                    replaceNode( tree, node, node[0] )
                    nodeList.append(node[0])
            else:
                for child in node:
                    nodeList.append(child)
    return tree

def isRoot( tree, node ):
    if findParent( tree, node ) == None:
        return True
    return False

def isPreterminal( node ):
    if isLeave( node ):
        return False
    # ie one child which id a leave
    children = [c for c in node]
    if len( children ) != 1:
        return False
    if not isLeave( children[0] ):
        return False
    return True

def isLeave( node ):
    # ie no children
    if len( [c for c in node] ) == 0:
        return True
    return False

# Small add: check if we have an EDU (to find EDU that are not leaves)
def isEDU( node ):
    try:
        i = int( node.label() )
        return True
    except:
        return False

def isPreEDU( node ):
    # Only for unary node
    children = [c for c in node]
    if len( children ) != 1:
        return False
    if not isEDU( children[0] ):
        return False
    return True

def checkLabels( tree ):
    queue = [tree]
    while queue:
        node = queue.pop()
        # Multiple labels created with binarization
        # --> The first label is the parent: we choose this label
        if '<' in node.label():
            node.set_label( node.label().split('|')[0] )
        # Multiple labels created when removing unary nodes
        # --> if all labels are the same, keep this label
        # --> if one of the collapsed label is the same as the one of the parent, rm it
        # --> else by default select the 2nd label (for root, always 2nd label)
        if "+" in node.label():
            label1, label2 = node.label().split('+')
            if label1 == label2:
                node.set_label( label1 )
            else:
                parent_node = findParent( tree, node )
                if parent_node == None:
                    # Keep the more local relation, ie the second one
                    node.set_label( label2 )
                else:
                    if parent_node.label() == label1:
                        node.set_label( label2 )
                    elif parent_node.label() == label2:
                        node.set_label( label1 )
                    else:
                        node.set_label( label2 )
        queue.extend( [c for c in node] )

def finalCheck( tree ):
    for treepos in tree.treepositions():
        children = [c for c in tree[treepos]]
        if not len( children ) in [0,2]:
            print( "Non binary tree, node:", tree[treepos].label(), ", #children", len( children ) )
            Tree.draw( tree[treepos] )
        if ">" in tree[treepos].label() or "+" in tree[treepos].label():
            print( "Not modified labels: ", tree[treepos].label() )
            Tree.draw( tree[treepos] )

# -- UTILS
def replaceNode( tree, node, child ):
    for treeposition in tree.treepositions():
        if tree[treeposition] == node:
            tree[treeposition] = child
            break

def findParent( tree, childNode ):
    queue = [tree]
    while queue:
        node = queue.pop()
        children = [c for c in node]
        for c in children:
            if c == childNode:
                return node
        queue.extend( [c for c in node] )
    return None

def checkWellformedness( strTree, type_="pred" ):
    try:
        tree = Tree.fromstring( strTree )
        return True
    except:
        if type_ == "gold":
            sys.exit( "Gold tree not well formed\n"+strTree )
        else:
            return False

# ----------------------------------------------------------------------------------
# EVALUATION (Yangfeng evaluation code + need to transform the trees into a bracketed format)
# ----------------------------------------------------------------------------------
def bracketForm( tree ):
    '''
    Transform to the bracketed form used by Yangfeng code
    - relation between X and Y-Z
        - if NS: X-X span ; Y-Z rel
        - if NN: X-X rel  ; Y-Z rel
    return list of (span, nuclearity, relation)
    '''
    brackets = []
    nodeList = [tree]
    # depth-first traversal of tree
    while nodeList != []:
        node = nodeList.pop()
        if isinstance(node,Tree):
            label = node.label()
            nuc = label.split('-')[0]
            relation = '-'.join( label.split('-')[1:] )
            children = [c for c in node]
            #print( label, len(children) )
            if len( children ) == 2:
                arg1, arg2 = getArgumentSpan( children[0], children[1] )

                if nuc.lower() == "ns":
                    brackets.append( (arg1, "nucleus", "span") )
                    brackets.append( (arg2, "satellite", relation) )
                elif nuc.lower() == "sn":
                    brackets.append( (arg1, "satellite", relation) )
                    brackets.append( (arg2, "nucleus", "span") )
                elif nuc.lower() == "nn":
                    brackets.append( (arg1, "nucleus", relation) )
                    brackets.append( (arg2, "nucleus", relation) )
                nodeList.extend( children )
            else:
                if not len( children ) in [0,2]:
                    sys.exit("Evaluation: non binary tree" )
    return brackets

class Performance(object):
    def __init__(self, percision, recall):
        self.percision = percision
        self.recall = recall

class Metrics(object):
    def __init__(self, levels=['span','nuclearity','relation']):
        """ Initialization

        :type levels: list of string
        :param levels: evaluation levels, the possible values are only
                       'span','nuclearity','relation'
        """
        self.levels = levels
        self.span_perf = Performance([], [])
        self.nuc_perf = Performance([], [])
        self.rela_perf = Performance([], [])

    # Modified take bracket directly instead of trees
    def eval(self, goldbrackets, predbrackets):
        """ Evaluation performance on one pair of RST trees

        :type goldtree: RSTTree class
        :param goldtree: gold RST tree

        :type predtree: RSTTree class
        :param predtree: RST tree from the parsing algorithm
        """
        for level in self.levels:
            if level == 'span':
                self._eval(goldbrackets, predbrackets, idx=1)
            elif level == 'nuclearity':
                self._eval(goldbrackets, predbrackets, idx=2)
            elif level == 'relation':
                self._eval(goldbrackets, predbrackets, idx=3)
            else:
                raise ValueError("Unrecognized evaluation level: {}".format(level))

    def _eval(self, goldbrackets, predbrackets, idx):
        """ Evaluation on each discourse span
        """
        goldspan = [item[:idx] for item in goldbrackets]
        predspan = [item[:idx] for item in predbrackets]
        allspan = [span for span in goldspan if span in predspan]
        p, r = 0.0, 0.0
        for span in allspan:
            if span in goldspan:
                p += 1.0
            if span in predspan:
                r += 1.0
        p /= len(goldspan)
        r /= len(predspan)
        if idx == 1:
            self.span_perf.percision.append(p)
            self.span_perf.recall.append(r)
        elif idx == 2:
            self.nuc_perf.percision.append(p)
            self.nuc_perf.recall.append(r)
        elif idx == 3:
            self.rela_perf.percision.append(p)
            self.rela_perf.recall.append(r)

    def report(self):
        """
        Compute the F1 score for different evaluation levels
            and print it out
        """
        global_scores = {}
        for level in self.levels:
            if 'span' == level:
                p = numpy.array(self.span_perf.percision).mean()
                r = numpy.array(self.span_perf.recall).mean()
                f1 = (2 * p * r) / (p + r)
                print( 'F1 score on span level is {0:.4f}'.format(f1) )
                global_scores['span'] = f1
            elif 'nuclearity' == level:
                p = numpy.array(self.nuc_perf.percision).mean()
                r = numpy.array(self.nuc_perf.recall).mean()
                f1 = (2 * p * r) / (p + r)
                print( 'F1 score on nuclearity level is {0:.4f}'.format(f1) )
                global_scores['nuc'] = f1
            elif 'relation' == level:
                p = numpy.array(self.rela_perf.percision).mean()
                r = numpy.array(self.rela_perf.recall).mean()
                f1 = (2 * p * r) / (p + r)
                print( 'F1 score on relation level is {0:.4f}'.format(f1) )
                global_scores['rel'] = f1
            else:
                raise ValueError("Unrecognized evaluation level")
        print( "\t".join( [str( round( global_scores[l]*100, 2 ) ) for l in ['span', 'nuc', 'rel']] ) )

if __name__ == '__main__':
    main()
