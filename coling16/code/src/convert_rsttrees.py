#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse, os, sys
from nltk import Tree
import numpy as np
from nltk.tokenize import treebank





def main( ):
    parser = argparse.ArgumentParser(
            description='Convert a discourse tree to a sequence, either from the\
                    constituent (original) format, or building another view of the \
                    trees: sequence from the dependency format and keeping nuclearity \
                    information or not. Allow to get the main data and the \
                    data for the multi-view experiments, as decribed in COLING16.')

    parser.add_argument('-i', '--inpath',
            dest='inpath',
            action='store',
            help='Directory containing the RST trees. The trees have to be in\
                    bracketed format (PTB like) and the extension has to be \
                    .dmrg, use pre-process script to convert.')
    parser.add_argument('-e', '--edus',
            dest='edus',
            action='store',
            help='Directory containing the EDUs files. The EDU files should have \
                    the same name as the corresponding tree file, there should \
                    be a stand off annotation of the text of the EDUS, and the \
                    extension should be .out.edus or .edus (original RST DT format).\
                    If not provided, EDU files have to be in inpath.')
    parser.add_argument('-o', '--outpath',
            dest='outpath',
            action='store',
            help='Directory containing the output files (.const et .dep)')
    parser.add_argument('-m', '--mode',
            dest='mode',
            action='store',
            default="dep",
            choices = ["dep", "const"],
            help='Mode of the conversion between: constituency/original trees (const)\
                    and dependency representation (dep). (Default dep)')
    parser.add_argument('--no_nuc',
            dest='nuclearity',
            action='store_false',
            help='Do not keep nuclearity information')
    parser.add_argument('--no_rel',
            dest='relation',
            action='store_false',
            help='Do not keep relation information')

    parser.set_defaults(nuclearity=True)
    parser.set_defaults(relation=True)

    args = parser.parse_args()

    keep_nuc, keep_rel = args.nuclearity, args.relation

    if not os.path.isdir( args.outpath ):
        os.mkdir( args.outpath )

    if not args.edus:#if EDUs files are in the same directory as the tree files
        args.edus = args.inpath

    readWriteRSTTrees( args.inpath, args.edus, args.outpath, args.mode, keep_nuc, keep_rel )


def readWriteRSTTrees( inpath, edus, outpath, mode, keep_nuc, keep_rel ):
    count_files, files = 0, set()
    for _file in [f for f in os.listdir( inpath ) if not f.startswith( '.')
            and f.endswith( ".dmrg" )]:
        basename = os.path.basename( _file ).split('.')[0]
        if basename in files:
#             print( "File already processed: "+basename+", ignore duplicates",
#                     file=sys.stderr )
            continue
        files.add( basename )
        count_files += 1
        print( "Reading:", basename, file=sys.stderr )
        edu2txt = readEdu( edus, basename )

        file_out = makeFileName( mode, keep_nuc, keep_rel, outpath, basename )
        tree = Tree.fromstring( open( os.path.join( inpath, _file ) ).read() )

        if mode == "const":# Constituent form (main + some aux views)
            constituent_seq = convert_constituent( tree, len( edu2txt ),
                    keep_nuc=keep_nuc, keep_rel=keep_rel )
            write_constituent( edu2txt, constituent_seq, file_out )
        elif mode == "dep":# Dependency form (aux view)
            root, dependencies = convert_dependency( tree, keep_nuc=keep_nuc,
                    keep_rel=keep_rel )
            write_dependency( edu2txt, root, dependencies, file_out )

#     print( "#Documents", count_files, len( files ), file=sys.stderr )


# ----------------------------------------------------------------------------------
# Dependency form
# ----------------------------------------------------------------------------------
def convert_dependency( tree, keep_nuc=True, keep_rel=True ):
    '''
    1) Choose the root EDU, first EDU that is a nucleus
        (an EDU is a nucleus iif it is in the salient set) e.g.:
        '[It's an interesting point,]_1 [because it defines...]_2': root 1
        '[Because it defines...,]_1' [it's an interesting point]_1: root 2
    2) Left headed among nuclei
       (find the sallient set for each child, ie all nuclei)
       ( NN-textual ( NN-same (NN-list (1,2) (NS-elab (3, NS-elab(4,5))), NN-list (...
       1 -list-> 2 , 1 -same-> 3 , 3 -elab-> 4 , 4 -elab-> 5
    '''
    relations = [] # [label, left sallient set, right sallient set], ..
    _dep_form( tree, relations, keep_nuc=keep_nuc )
    root = set_root( relations ) # first nucleus in the document
    added_edus = set() # check all edus have been covered
    dependencies = { root:[] } # build dependency dict: head --> [ [rel, dep], ...]
    for ( r, left, right ) in relations: # left and right are sallient ens, ie nuclei only
        if root in left:
            head = root
        else:
            head = left[0]
        dep = right[0]
        added_edus.add( head )
        added_edus.add( dep )
        if head in dependencies:
            dependencies[head].append( [r, dep] )
        else:
            dependencies[head] = [ [r,dep] ]
#     print( "Choosen root:", root, file=sys.stderr )
    return root, dependencies

def write_dependency( edu2text, root, dependencies, file_out ):
    '''
    Write a new file, format: edutext TAB label
    '''
    sorted_edus = sorted( edu2text )
    with open( file_out, "w" ) as f:
        for edu in sorted_edus:
            if edu == root:
                f.write( edu2text[edu]+"\t"+"root"+"\n" )
            else:
                find_dep_link( edu, dependencies, sorted_edus, edu2text, f )

def find_dep_link( edu, dependencies, sorted_edus, edu2text, f ):
    '''
    Get the dependency link for edu
    '''
    for h in dependencies:
        for (r,d) in dependencies[h]:
            if edu == d:
                f.write( "\t".join( [edu2text[edu],
                    str(sorted_edus.index( h )-sorted_edus.index( d ))+"_"+r] )+"\n" )

def set_root( relations ):
    '''
    Return the index of the root nucleus of the tree
    '''
    nuclei = []
    for ( r, left, right ) in relations:
        nuclei.extend( [e for e in left] )
        nuclei.extend( [e for e in right] )
    return sorted( nuclei )[0]

# Depth-first tree traversal
def _dep_form( t, relations, keep_nuc=False ):
    try:
        label = t.label()
        label = label.replace('_', '')
        if not keep_nuc and ("NN-" in t.label() or "NS-" in t.label() or "SN-" in t.label()):
            label = t.label().split('-')[0] # keep only nuc
        if not label == "EDU":
            children = [c for c in t]
            if len( children ) > 2:
                sys.exit( "non binary tree ?" )
            # looking for sallient ensemble for each child
            if "NN-" in t.label() or "NS-" in t.label():
                salient_left = get_salient( children[0] )
                salient_right = get_salient( children[1] )
            elif "SN-" in t.label():
                salient_left = get_salient( children[1] )
                salient_right = get_salient( children[0] )
            relations.append( [label, salient_left, salient_right] )
            _dep_form( children[0], relations, keep_nuc=keep_nuc )
            _dep_form( children[1], relations, keep_nuc=keep_nuc )
    except AttributeError:
        return

def get_salient( tree ):
    salient_set = []
    _get_salient( tree, salient_set )
    return salient_set

def _get_salient( t, salient_set ):
    try:
        label = t.label()
        children = [c for c in t]
        if label == "EDU":
            salient_set.append( int( children[0] ) )
        else:
            nuclearity = label.split('-')[0]
            left_nuc = nuclearity[0]
            right_nuc = nuclearity[1]
            if left_nuc == 'N':
                _get_salient( children[0], salient_set )
            if right_nuc == 'N':
                _get_salient( children[1], salient_set )

    except AttributeError:
        return



# ----------------------------------------------------------------------------------
# -- Constituent form
# ----------------------------------------------------------------------------------
def convert_constituent( tree, nb_edus, keep_nuc=True, keep_rel=True ):
    const_seq, cur_seq = [],[]
    closed = ''
    edus = set()
    _const_form( tree, const_seq, edus, cur_seq, closed, keep_nuc=keep_nuc, keep_rel=keep_rel )
    if nb_edus != len( edus ):
        sys.exit( "Inconsistent number of EDUs" )
    edu2str_constituents = _add_closing_parenthesis( const_seq )# Add closing parenthesis
    return edu2str_constituents

def write_constituent( edu2text, const_seq, file_out ):
    with open( file_out, 'w' ) as f:
        for edu in sorted( const_seq ):
            f.write( edu2text[edu].strip()+"\t"+const_seq[edu].strip()+"\n" )

def _add_closing_parenthesis( const_seq ):
    edu2str_const = {}
    cnt_open = 0
    cnt_close = 0
    for i,seq in enumerate( const_seq ):
        if len( seq ) == 2 :
            str_ ="("+"(".join( seq[-1] )
        else:
            str_ = "".join( [str(e) for e in seq[1:]] )

        for c in str_:
            if c == '(':
                cnt_open += 1
            elif c == ')':
                cnt_close += 1
        edu2str_const[int(seq[0])] = str_
    if cnt_open != cnt_close:
        sys.exit( "Inconsistent number of parenthesis " )
    return edu2str_const

# Depth-first tree traversal
def _const_form( t, const_seq, edus, cur_seq, closed, right=False, keep_nuc=True, keep_rel=True ):
    try:
        label = t.label()

        label = transform_label( label, keep_nuc, keep_rel )
        if right:
            closed += ')'
        else:
            closed = ''
        children = [c for c in t]
        if len( children ) == 2:
            cur_seq.append( label )
            if children[0].label() == "EDU":
                const_seq.append( [children[0].leaves()[0], cur_seq] )
                edus.add( children[0].leaves()[0] )
                cur_seq = []
            if children[1].label() == "EDU":
                const_seq.append( [children[1].leaves()[0], label, ')'+closed] )
                edus.add( children[1].leaves()[0] )
            _const_form( children[0], const_seq, edus, cur_seq, closed, right=False,
                    keep_nuc=keep_nuc, keep_rel=keep_rel )
            cur_seq = []
            _const_form( children[1], const_seq, edus, cur_seq, closed, right=True,
                    keep_nuc=keep_nuc, keep_rel=keep_rel)
        elif len( children ) > 2:
            sys.exit( "non binary tree ?" )
    except AttributeError:
        return

def transform_label( label, keep_nuc, keep_rel ):
    label = label.replace('_', '')
    # Remove -s -e -n suffixes in RSTDT labels
    if label.endswith( '-s-e' ) or label.endswith( '-n-e' ):
        label = label[:-4]
    if label.endswith( '-n' ) or label.endswith( '-e' ) or label.endswith( '-s' ):
        label = label[:-2]
    if keep_nuc and keep_rel:
        return label
    else:
        if "NN-" in label or "NS-" in label or "SN-" in label:#do not change EDU label
            # There could be - used in the relation name
            nuc, rel = label.split('-')[0], '-'.join( label.split('-')[1:] )
            if keep_nuc == False:
                label = rel
            elif keep_rel == False:
                label = nuc
            else:#both false? never tried
                label = ''
        return label


# -- EDU utils
def readEdu( edus, basename ):
    '''
    Get the file correspodning to basename containing the EDUs in the directory edus
    Return a dictionary: EDU id -> text of the EDU
    '''
    edus_file = os.path.join( edus, basename+'.out.edus' ) if os.path.isfile( os.path.join( edus, basename+'.out.edus' ) ) else os.path.join( edus, basename+'.edus' )
    if not os.path.isfile( edus_file ):
        sys.exit( "EDUs file not found: "+edus_file )
    return getText_edus( edus_file )


def getText_edus( edus_file ):
    '''
    Return a dictionary: EDU id : text of the EDU from a file
    '''
    tokenizer = treebank.TreebankWordTokenizer()
    edu2text = {}
    with open( edus_file ) as myfile:
        _lines = myfile.readlines()
        for i in range( len( _lines ) ):
            tokens = tokenizer.tokenize( _lines[i].strip() )# tokenization
            edu2text[i+1] = " ".join( tokens )# EDU index begins at 1
    return edu2text

# -- Utils files
def makeFileName( mode, keep_nuc, keep_rel, outpath, basename ):
    '''
    Return the path to the file, the name takes into account the mode and the info
    kept (nuc and rel)
    '''
    file_out =  os.path.join( outpath, basename.split('.')[0] )
    ext = '.seq'# because converted to a sequence
    if keep_nuc:
        ext += '.nuc'
    if keep_rel:
        ext += '.rel'
    if mode == 'const':
        ext += '.const'
    if mode == 'dep':
        ext += '.dep'
    return file_out+ext


if __name__ == '__main__':
    main(  )
