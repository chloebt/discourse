#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse, os, sys, shutil, codecs
#from nltk import Tree
import numpy as np
#from nltk.tokenize import treebank
#from nltk.corpus import BracketParseCorpusReader
from xml.etree import ElementTree as ET


def main( ):
    parser = argparse.ArgumentParser(
            description='Convert data into the sequential format used for the \
                    multi-task experiments (COLING16). Data come from Factbank, \
                    Ontonotes and the Santa Barbara corpus. See the paper for \
                    more information on the information used from these data.')

    parser.add_argument('-a', '--annot',
            dest='annot',
            action='store',
            help='Directory containing the data')
    parser.add_argument('-o', '--outdir',
            dest='outdir',
            action='store',
            help='Directory containing the output files')
    parser.add_argument('-t', '--datatype',
            dest='datatype',
            action='store',
            choices = ["factbank", "coref", "speech"],
            help='Type of annotations read')
    parser.add_argument('-m', '--mode',
            dest='mode',
            action='store',
            default="majority",
            choices = ["majority", "first", "fine"],
            help='Mode for choosing the labels (from Factuality/Timebank data:\
                    choose the most frequent one (majority), the first \
                    occuring in the sentence (first) or keep all the different \
                    labels (fine) (Default=majority)')

    args = parser.parse_args()

    if not os.path.isdir( args.outdir ):
        os.mkdir( args.outdir )

    if args.datatype == "factbank": # Factbank v1
        readWriteFactBank( args.annot, args.outdir, mode=args.mode )
    elif args.datatype == 'coref': # Ontonote
        readWriteCoref( args.annot, args.outdir )
    elif args.datatype == 'speech': # Santa Barbara corpus
        readWriteSpeech( args.annot, args.outdir )




# ----------------------------------------------------------------------------------
# Santa Barbara corpus (speech), talk turns
# ----------------------------------------------------------------------------------
def readWriteSpeech( annotDir, outdir ):
    outdir = os.path.join( outdir, "speech-santabarbara" )
    if not os.path.isdir( outdir ):
        os.mkdir( outdir )

    speakers = set()
    speakers_ignored = set()
    max_len = 100 #keep max_len turn to form a document

    for _file in [f for f in os.listdir( annotDir ) if not f.startswith( '.')]:
        count_doc = 0
        outfile = os.path.join( outdir, _file.split('.')[0]+"D"+str(count_doc)+".speech" )
        with open( os.path.join( annotDir, _file ) ) as f:
            c = open( outfile, 'w' )
            count_line = 0
            lines = f.readlines()
            cur_speaker = None
            for l in lines:
                l = l.strip()
                tokens, speaker = l.split('\t')
                speaker = speaker.strip()
                if '>' in speaker: #Remove strange caracters from speaker names
                    speaker = speaker.replace( '>', '' )
                if '~' in speaker:
                    speaker = speaker.replace( '~', '' )
                if '#' in speaker:
                    speaker = speaker.replace( '#', '' )
                if ':' in speaker:
                    speaker = speaker.replace( ':', '' )

                if speaker.isalpha() == True or speaker[:-1].isalpha() == True: #check not a strange speaker name
                    speakers.add( speaker )
                    if speaker == cur_speaker:
                        label = 'I-turn' #begins the turn of current speaker
                    else:
                        label = 'B-turn' #ends the turn of current speaker
                        cur_speaker = speaker
                    c.write( '\t'.join( [tokens.strip(), label] )+'\n' )
                else:
                    speakers_ignored.add( speaker ) # speakers with non alpha names are ignored
                count_line += 1

                if count_line == max_len:
                    c.close()
                    count_line = 0
                    count_doc += 1
                    outfile = os.path.join( outdir, _file.split('.')[0]+"D"+str(count_doc)+".speech" )
                    c = open( outfile, 'w' )
                    cur_speaker = None
            print( "Count turns", _file, count )
    print( "#speakers", len(speakers), "#ignored speakers", len(speakers_ignored) )


# ----------------------------------------------------------------------------------
# ONTONOTE - COREF
# ----------------------------------------------------------------------------------
def readWriteCoref( annotDir, outdir ):
    outdir = os.path.join( outdir, "coref-ontonotes" )
    if not os.path.isdir( outdir ):
        os.mkdir( outdir )
    if os.path.isfile( annotDir ):
        document = _readCoref( annotDir )
        writeCoref( document, os.path.basename(annotDir), outdir )
    else:
        ext = '.coref'
        path = os.path.join( annotDir, "annotations" )
        if not os.path.isdir( path ):
            sys.exit( "Data directory not found: "+path )
        for p, dirs, files in os.walk( path ):
            subdir = os.path.basename(p)
            if not '/bc/' in p and not '/wb/' in p:#ignoring conversations and web data
                dirs[:] = [d for d in dirs if not d[0] == '.']
                for file in files:
                    if file.endswith( ext ) and not file.startswith('.'):
                        try:
                            documents = _readCoref( os.path.join( p, file ) )
                            writeCoref( documents, file, outdir )
                        except:
                            raise

def writeCoref( documents, file, outdir ):
    for document in documents:
        try:
            docno = document.doc_number
            outpath = os.path.join( outdir, file.replace( '.coref', '_p'+str(docno)+'.coref' ) )
            c = open( outpath, 'w' )
            for i,sentence in enumerate( document.sentences ):
                # Remove trace
                raw_sentence = ' '.join( [t for t in sentence.text.strip().split() if not '*' in t] )
                raw_sentence = raw_sentence.replace( '-LRB-', '(' )
                raw_sentence = raw_sentence.replace( '-RRB-', ')' )
                raw_sentence = raw_sentence.replace( ' 0 ', ' ' )#trace?
                c.write( raw_sentence.strip()+'\t'+sentence.coreflabel+'\t'+str(i)+'\n' )
            c.close()
        except:
            print( 'Pb with file', document.path, docno )
            os.remove( outpath )
            continue

def _readCoref( _file ):
    documents,nodoc = [],0
    tree = ET.parse(_file)
    root = tree.getroot()
    for part in root.iter('TEXT'):#a doc may be split into several parts, each = 1 coref chain
        partno = part.attrib['PARTNO']
        document = CorefDocument( _file, partno )#Create a new document instance
        pos = 0 #current caracter offset
        if part.text:
            document.text += part.text
            pos += len( part.text )
        children = [c for c in part]
        pos = _trav( children, _file, pos, document, None )#rec traversal of children
        if len(document.text) != pos:
            print( document.text )
            sys.exit( 'len(text) != pos in '+_file+' ('+str(len(document.text))+' vs '+str(pos) )
        setSentences( document, partno )
        documents.append( document )
    return documents

def setSentences( document, partno ):
    k = 0
    raw_sentences = [ s for s in document.text.split('\n') if s.strip() != '']
    for j,raw_sentence in enumerate( raw_sentences ):
        sentence = CorefSentence( raw_sentence, j )
        for i in range( k,k+len(raw_sentence) ):
            mentions = getMentions( document.corefMentions, i )
            for m in mentions:
                m.sentence = j
                sentence.corefMentions.append( m )
        if j == 0:
            sentence.coreflabel = 'ROOT'
        else:
            sent_mentionsId = set([m.id for m in sentence.corefMentions])
            prevSent_mentionsId = set([m.id for m in document.sentences[j-1].corefMentions])
            prevText_mentionsId = set([m.id for s in document.sentences[:j] for m in s.corefMentions])
            if len( sent_mentionsId.intersection( prevSent_mentionsId ) ) != 0:
                sentence.coreflabel = 'COREF_PREV'
            elif len( sent_mentionsId.intersection( prevText_mentionsId ) ) != 0:
                sentence.coreflabel = 'COREF_OTHER'
        k += len(raw_sentence)
        document.sentences.append( sentence )


def _trav( children, _file, pos, document, current_mention ):
    '''
    Lack adding tail for parent of the parent of the actual child, see for ex
    data/corpora/others/ontonotes/english/annotations/bn/abc/00/abc0001.coref
    <COREF ID="37" TYPE="IDENT">one of <COREF ID="38" TYPE="IDENT"><COREF ID="16" TYPE="IDENT">the town 's</COREF> two meat - packing plants</COREF></COREF>
    37 is only "one of" for now, the others are ok
    '''
    for child in children:
        cm = CorefMention( '', _file )
        cm.begin = pos
        if 'ID' in child.attrib:
            cm.id = child.attrib['ID']
        if child.text != None:
            pos += len( child.text )
            cm.text += child.text
            document.text += child.text
            cm.end = pos
            if current_mention != None:
                current_mention.text += child.text
        childrenOfChild = [c for c in child]
        if len( childrenOfChild ) != 0:
            pos = _trav( [c for c in child], _file, pos, document, cm )
        if child.tail:
            document.text += child.tail
            pos += len(child.tail)
            if current_mention != None:
                current_mention.text += child.tail
        document.corefMentions.append( cm )
    return pos

def getMentions( corefMentions, i ):
    """ Return the mentions beginning at offset i """
    mentions = []
    for m in corefMentions:
        if m.begin == i:
            mentions.append( m )
    return mentions

class CorefMention:
    def __init__( self, text, document ):
        self.text = text
        self.id = None
        self.document = document
        self.begin = -1
        self.end = -1
        self.sentence = None

    def __str__( self ):
        return '\t'.join( [self.id, self.text] )

class CorefSentence:
    def __init__( self, text, sentno ):
        self.text = text
        self.sentence_number = sentno
        self.corefMentions = []
        self.coreflabel = 'NONE'

class CorefDocument:
    def __init__( self, path, docno ):
        self.path = path
        self.doc_number = docno#each part of a file corresp to a coreference chain
        self.text = ''
        self.sentences = []
        self.corefMentions = []

def getText( _file ):
    ''' Get the text without annotation, to check '''
    text = ''
    part = None
    part2text = {}
    with open( _file ) as f:
        lines = f.readlines()
        for l in lines[1:]:
            if '<TEXT PARTNO=' in l:
                part = l.split('=')[1].replace('>', '' )[1:-2].strip()
                part2text[part] = ''
            stop = False
            for i,c in enumerate( l ):
                if c == '<':
                    stop = True

                if not stop:
                    part2text[part] += c
                if c == '>':
                    stop = False
    return part2text



# ----------------------------------------------------------------------------------
# FACTBANK/TIMEBANK
# ----------------------------------------------------------------------------------
def readWriteFactBank( annotDir, outdir, sep = "|||", mode='majority' ):
    '''
    annotDir/annotation/ contain several interesting files, each can be converted in
    a sequence prediction task (or we could combine some of them?)
        - tml_event.txt: event class (e.g. occurrence, state, reporting ...)
        - tml_timex3: typex type (e.g. date, duration, time ...)
        - tml_instance: tense, aspect, polarity (+ev. modality, cardinality)
        - tml_tlink: before, after ...
        - tml_slink: evidential, modal ...
        - tml_alink: initiate, continue ...
        - fb_factValue: factuality (e.g. certain, probable, possible ...)

    Plus, we need to read:
        - sentences.txt: contains the text of each sentence associated with its ID
        - fb_event: eID and eiID of each event instance
    '''
    factBank = FactBankCorpus( annotDir, sep=sep )
    writeData( factBank, outdir, mode=mode )

def writeData( factBank, outdir, mode='majority' ):
    '''
    - For 'modality', with 'fine' mode, most of the labels are NONE

     VP chunk with label eg B-PAST+can
    '''
    for dataset in [ 'tense', 'modality', 'polarity', 'factuality', 'aspect' ]:
        # 'tense', 'modality', 'polarity', 'factuality', 'aspect']:
        out_subdir = os.path.join( outdir, dataset+'_'+mode )
        if not os.path.isdir( out_subdir ):
            os.mkdir( out_subdir )
        for document in factBank.documents:
            fpath = os.path.join( out_subdir, document._path+'.'+dataset )
            c = open( fpath, 'w' )
            for sentence in document.sentences[1:]: #ignore fisrt sentence (=title)
                label = getLabel( dataset, sentence, mode=mode )
                c.write( '\t'.join( [' '.join( [token.raw for token in sentence.tokens] ),
                    label, str(sentence.sent_number)] )+'\n' )
            c.close()

def getLabel( dataset, sentence, mode='majority' ):
    labels = [token.annot[dataset] for token in sentence.tokens if dataset in token.annot]
    if dataset == 'factuality':
        return _getLabelFactuality( labels, mode=mode )
    else:
        return _getLabel( labels, mode=mode )


def _getLabelFactuality( labels, mode='majority' ):
    # A little bit more complicated: different values for different sources
    # labels: list of list (one per word) of 4-uplet (one per source)
    # 4-uplet: (id, token, source, fact value)
    label = 'NONE'
    if len( labels ) != 0:
        if mode == 'majority':
            label2freq = getLabelFreqList( labels )
            sorted_labels = sorted( label2freq.items(), key=lambda x:x[1], reverse=True )
            if len( label2freq.keys() ) == 1:
                label = sorted_labels[0][0]
            else:
                ordered_labels = [] # try to pick a label that is not NONE
                for a in labels:
                    for l in a:
                        if l[-1] != 'NONE':
                            ordered_labels.append( l[-1] )
                # Choose the most frequent != None
                label = sorted_labels[0][0] if sorted_labels[0][0].lower() != 'uu' else sorted_labels[1][0]
                # if equality, choose the first occuring
                if len( ordered_labels ) != 0 and label != ordered_labels[0]:
                    for l in ordered_labels:
                        if label != l and label2freq[label] == label2freq[l]:
                            label = l
                            break
        elif mode == 'first':
            label = labels[0][0][-1]
        elif mode == 'fine':
            reduced_labels = []
            for l in labels:
                if not l[-1] in reduced_labels:
                    reduced_labels.append( l[-1] )
            label = '-'.join( reduced_labels )
    return label

def _getLabel( labels, mode='majority' ):
    label = 'NONE'
    if len( labels ) == 1:
        label = labels[0]
    elif len( labels ) > 1:
        if mode == 'majority':
            label2freq = getLabelFreq( labels )
            sorted_labels = sorted( label2freq.items(), key=lambda x:x[1], reverse=True )
            if len( label2freq.keys() ) == 1:
                label = sorted_labels[0][0]
            else:
                ordered_labels = []
                for l in labels:
                    if l != 'NONE':
                        ordered_labels.append( l )
                # Choose the most frequent != None
                label = sorted_labels[0][0] if sorted_labels[0][0] != 'NONE' else sorted_labels[1][0]
                # if equality, choose the first occuring
                if len( ordered_labels ) != 0 and label != ordered_labels[0]:
                    for l in ordered_labels:
                        if label != l and label2freq[label] == label2freq[l]:
                            label = l
                            break
        elif mode == 'first':
            labels = [l for l in labels if l != 'NONE']
            if len( labels ) == 0:
                label = 'NONE'
            else:
                label = labels[0]
        elif mode == 'fine':
            label = '-'.join( labels )
    return label

def getLabelFreq( labels ):
    ''' labels is a list '''
    tlabels = {}
    for t in labels:
        if t in tlabels:
            tlabels[t] += 1
        else:
            tlabels[t] = 1
    return tlabels

def getLabelFreqList( labels ):
    ''' labels is a list of tuples '''
    tlabels = {}
    for t in labels:
        for a in t:
            if a[-1] in tlabels:
                tlabels[a[-1]] += 1
            else:
                tlabels[a[-1]] = 1
    return tlabels

class Corpus:
    def __init__( self, _path ):
        self._path = _path

    def read( self ):
        raise NotImplementedError

class FactBankCorpus( Corpus ):
    def __init__( self, _path, sep='|||' ):
        Corpus.__init__( self, _path )
        self.sep = sep
        # base info files
        self.annotPath = os.path.join( self._path, "annotation" )
        self.sentencesFile = os.path.join( self.annotPath, "sentences.txt" )
        self.filesNames = os.path.join( self.annotPath, "files.txt" )
        self.tokensFile = os.path.join( self.annotPath, "tokens_ling.txt" )
        self.tokenSpan = os.path.join( self.annotPath, "offsets.txt" )
        self.tokensTml =  os.path.join( self.annotPath, "tokens_tml.txt" )
        self.fbEvent = os.path.join( self.annotPath, "fb_event.txt" )

        self.tmlEvent = os.path.join( self.annotPath, "tml_event.txt" )
        self.tmlTimex3 = os.path.join( self.annotPath, "tml_timex3.txt" )
        self.tmlInstance = os.path.join( self.annotPath, "tml_instance.txt" )
        self.fbValue = os.path.join( self.annotPath, "fb_factValue.txt" )

        # Get all the distinct files
        self.files = set()
        self.getFileNames( )

        self.documents = []

        # Read the info files
        self.buildCorpus()
        # Read the annot files
        self.annotCorpus()

    def buildCorpus( self ):
        self.readSentences( )
        self.readTokens( )
        self.readTokensSpan( )
        self.readTokensTml( )
        self.read_fbEvent() # get eiid

    def annotCorpus( self ):
        self.read_tmlEvent()
        self.read_tmlTimex3()
        self.read_tmlInstance()#need to have an eiid associated with each token
        self.read_fbValue()
        ##self.read_fbCorefSource()

    def getDocument( self, filename ):
        for d in self.documents:
            if d._path == filename:
                return d
        return None

    # ------
    def read_tmlEvent( self ):
        with open( self.tmlEvent ) as f:
            for l in f.readlines():
                l = l.strip()
                filename, sentno, eID, eClass, eText = [ clean(x) for x in l.split(self.sep) ]
                document = self.getDocument( filename )
                sentence = document.getSentence( int(sentno) )
                tokens = sentence.getTokenEvent( eID )
                if len( tokens ) == 0:
                    #print( filename, sentno, eID, eClass, eText )
                    continue
                for token in tokens:
                    token.annot["eclass"] = eClass
                    token.annot["eText"] = eText

    def read_tmlTimex3( self ):
        with open( self.tmlTimex3 ) as f:
            for l in f.readlines():
                l = l.strip()
                filename, sentno, tID, type_, value, mod, functionInDoc, temporalFunction, anchorTimeId, beginPoint, endPoint, freq, quant, timexText, tokenText = [ clean(x) for x in l.split(self.sep) ]
                document = self.getDocument( filename )
                sentence = document.getSentence( int(sentno) )
                tokens = sentence.getTokenEvent( tID )
                if value == '':
                    value = 'NONE'
                if beginPoint == '':
                    beginPoint = 'NONE'
                if anchorTimeId == '':
                    anchorTimeId = 'NONE'
                if quant == '':
                    quant = 'NONE'
                if freq == '':
                    freq = 'NONE'
                if mod == '':
                    mod = 'NONE'
                if endPoint == '':
                    endPoint = 'NONE'
                if len( tokens ) == 0:
                    #print( filename, sentno, eID, eClass, eText )
                    continue
                for token in tokens:
                    token.annot["tm3type"] = type_
                    token.annot["tm3value"] = value
                    token.annot["tm3mod"] = mod
                    token.annot["tm3functionInDoc"] = functionInDoc
                    token.annot["tm3temporalFunction"] = temporalFunction
                    token.annot["tm3anchorTimeId"] = anchorTimeId
                    token.annot["tm3beginPoint"] = beginPoint
                    token.annot["tm3endPoint"] = endPoint
                    token.annot["tm3freq"] = freq
                    token.annot["tm3quant"] = quant
                    token.annot["tm3timexText"] = timexText
                    token.annot["tm3tokenText"] = tokenText
        return

    def read_tmlInstance( self ):
        with open( self.tmlInstance ) as f:
            for l in f.readlines():
                l = l.strip()
                filename, eId, eiId, tense, aspect, pos, polarity, modality, cardinality, signalId = [ clean(x) for x in l.split(self.sep) ]
                document = self.getDocument( filename )
                tokens = document.getTokenEvent( eId )
                if modality == '':
                    modality = 'NONE'
                if cardinality == '':
                    cardinality = 'NONE'
                if signalId == '':
                    signalId = 'NONE'
                if len( tokens ) == 0:
                    #print( filename, eId, eiId, tense, aspect, pos, polarity, modality, cardinality, signalId )
                    continue
                for token in tokens:
                    token.annot["tmleiId"] = eiId
                    token.annot["tense"] = tense
                    token.annot["aspect"] = aspect
                    token.annot["pos"] = pos
                    token.annot["polarity"] = polarity
                    token.annot["modality"] = modality
                    token.annot["cardinality"] = cardinality
                    token.annot["signalId"] = signalId

    def read_fbValue( self ):
        with open( self.fbValue ) as f:
            for l in f.readlines():
                l = l.strip()
                filename, sentno, fvId, eId, eiId, relsourceId, eText, relSourceText, factValue = [ clean(x) for x in l.split(self.sep) ]
                document = self.getDocument( filename )
                sentence = document.getSentence( int(sentno) )
                tokens = sentence.getTokenEvent( eId )

                if len( tokens ) == 0:
                    #print( filename, sentno, eID, eClass, eText )
                    continue
                for token in tokens:
                    if not "factuality" in token.annot:#may be several value depending on the source
                        token.annot["factuality"] = []
                    token.annot["factuality"].append( [relsourceId, eText, relSourceText, factValue] )

    # ---
    def getFileNames( self ):
        with open( self.filesNames ) as f:
            for l in f.readlines():
                l = l.strip()
                filename, _ = [ clean(x) for x in l.split(self.sep) ]
                self.files.add( filename )

    def readSentences( self ):
        file2doc = {}
        with open( self.sentencesFile ) as f:
            for l in f.readlines():
                l = l.strip()
                filename, sentno, text = [ clean(x) for x in l.split(self.sep) ]
                sentno = int( sentno )
                if not filename in file2doc:
                    document = Document( filename )
                    file2doc[filename] = document
                file2doc[filename].sentences.append( Sentence( sentno, text ) )
        for f in file2doc:
            file2doc[f].sortSentences( )
            self.documents.append( file2doc[f] )


    def readTokens( self ):
        with open( self.tokensFile ) as f:
            for l in f.readlines():
                l = l.strip()
                filename, sentno, tokenNo, raw, pos = [ clean(x) for x in l.split(self.sep) ]
                sentno = int( sentno )
                tokenNo = int( tokenNo )
                document = self.getDocument( filename )
                sentence = document.getSentence( int(sentno) )
                if raw.strip() != '':
                    token = Token( int(tokenNo), raw.replace( '\\', ''), pos=pos )
                    sentence.tokens.append( token )
        for d in self.documents:
            for s in d.sentences:
                s.sortTokens()

    def readTokensTml( self ):
        with open( self.tokensTml ) as f:
            for l in f.readlines():
                l = l.strip()
                filename, sentno, tokno, raw, tmltag, tmltagid, tmltagloc = [ clean(x) for x in l.split(self.sep) ]
                document = self.getDocument( filename )
                sentence = None
                if sentno != 'NULL':
                    sentence = document.getSentence( int(sentno) )
                if sentence == None:
                    continue
                token = sentence.getToken( int(tokno) )
                if token == None:
                    continue
                token.tmltag = tmltag
                token.tmltagid = tmltagid
                token.tmltagloc = tmltagloc
                #print tmltag, tmltagid, tmltagloc

    def readTokensSpan( self ):
        with open( self.tokenSpan ) as f:
            for l in f.readlines():
                l = l.strip()
                filename, begin, end, sentno, tokno, raw = [ clean(x) for x in l.split(self.sep) ]
                document = self.getDocument( filename )
                sentence = None
                if sentno != 'NULL':
                    sentence = document.getSentence( int(sentno) )
                if sentence == None:
                    continue
                token = sentence.getToken( int(tokno) )
                if token == None:
                    continue
                token.span = tuple( [int(begin),int(end)] )

    def read_fbEvent( self ):
        with open( self.fbEvent ) as f:
            for l in f.readlines():
                l = l.strip()
                filename, sentno, eid, eiid, etext = [ clean(x) for x in l.split(self.sep) ]
                document = self.getDocument( filename )
                sentence = None
                if sentno != 'NULL':
                    sentence = document.getSentence( int(sentno) )
                if sentence == None:
                    continue
                tokens = sentence.getTokenEvent( eid )
                if len( tokens ) == 0:
                    #print( filename, sentno, eid, etext )
                    continue
                event = document.getEvent( eiid )
                if event == None:
                    event = EventInstance( eiid, etext, document, sentence )
                event.tokens = tokens
                for token in tokens:
                    token.event = event

    # ------
    def __str__( self ):
        str_ = ''
        for d in self.documents:
            str_ += '\n'+d.__str__()
        return str_




# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
class Document:
    def __init__( self, _path ):
        self._path = _path
        self.sentences_id = []
        self.sentences = []
        self.tokens = []
        self.events = [] # list of event instance, can be formed by several tokens

    def sortSentences( self ):
        self.sentences = sorted( self.sentences, key=lambda x:x.sent_number )
        ###self.sentences = sorted( self.sentences, lambda x,y:cmp(x.sent_number, y.sent_number) )
        self.sentences_id = [s.sent_number for s in self.sentences]

    def getSentence( self, sentno ):
        return self.sentences[self.sentences_id.index(sentno)] if sentno in self.sentences_id else None

    def getEvent( self, eiid ):
        for e in self.events:
            if e.eiid == eiid:
                return e
        return None

    def getTokenEvent( self, eID ):
        tokens = []
        for sentence in self.sentences:
            for token in sentence.tokens:
                if token.tmltagid == eID:
                    tokens.append( token )
        return tokens

    def __str__( self ):
        str_ = '\n'+self._path
        for sid in self.sentences_id:
            str_+="\n\n"+str(sid)+'\t'+self.sentences[self.sentences_id.index(sid)].__str__()
        return str_



# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
class Sentence:
    def __init__( self, num, raw ):
        self.document = None
        self.raw = raw
        self.sent_number = num#index of the sentence in the document
        self.tokens = []
        self.span = []

    def sortTokens( self ):
        self.tokens = sorted( self.tokens, key=lambda x:x.token_number )
        ###self.tokens = sorted( self.tokens, lambda x,y:cmp(x.token_number, y.token_number) )

    def getToken( self, tokno ):
        for t in self.tokens:
            if t.token_number == tokno:
                return t
        return None

    def getTokenEvent( self, eID ):
        tokens = []
        for t in self.tokens:
            if t.tmltagid == eID:
                tokens.append( t )
        return tokens

    def __str__( self ):
        #return ' '.join( [(str(t.token_number)+":"+t.raw) for t in self.tokens] )
        return self.raw+'\n'+'\n'.join( [t.__str__() for t in self.tokens] )


# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
class Token:
    def __init__( self, num, raw, pos=None ):
        self.document = None
        self.sentence = None
        self.pos = pos
        self.token_number = num#index of the token in the sentence
        self.doc_number = None#TODO?
        self.raw = raw
        self.span = None
        # Annotations
        self.annot = {}
        self.tmltag = None
        self.tmltagid = None # may have more than one token with the same
        self.tmltagloc = None
        #token.sourceid = None
        #self.eiid = None
        self.event = None

    def __str__( self ):
        str_ = str(self.token_number)+":"+self.raw
        str_ += ','+str(self.tmltag)+"-"+str(self.tmltagid)
        if len( self.annot ) != 0:
            str_ += '\n'.join( ['\t'+key+'='+str(val) for (key,val) in self.annot.items()] )
        if self.event != None:
            str_ += " (Event="+self.event.__str__()+') '
        return str_


class EventInstance:
    def __init__( self, eiid, raw, document, sentence ):
        self.eiid = eiid
        self.raw = raw
        self.document = document
        self.sentence = sentence
        self.tokens = []

    def __str__( self ):
        return self.eiid+":"+self.raw

def clean( text ):
    '''
    Elements in factBank are between ' and ' (not the sent numb)
    '''
    if text.startswith( '\'' ) and text.endswith( '\'' ):
        return ''.join( text[1:-1] )
    return text



if __name__ == '__main__':
    main(  )
