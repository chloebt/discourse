#!/bin/bash

# convert RST trees to sequences:
# - main task: sequences from the constituent/original tree
# - auxiliary views:
#       - dependency: convert the tree to a dependency graph, represented by 
#       sequences of EDUs linked to their head
#       - nuclearity: remove nuclearity information from the original trees
#       (keep only discourse relations)
#       - relation: remove relation information from the original trees
#       (keep only nuclearity)
#       --> to get the fine/coarse grained views, use the mapping from Carlson et al.
#       on the original trees
# output files format:
#   sentence \t label \t id-sentence


# SRC=/Users/chloebraud/projects/navi/sandbox/multitask-view/
# RSTDT_bracket=/Users/chloebraud/projects/navi/sandbox/multitask-view/rstdt-sample-bracketed/
# OUTDIR_main=/Users/chloebraud/projects/navi/sandbox/multitask-view/main-task-data-sample/
# OUTDIR_aux=/Users/chloebraud/projects/navi/sandbox/multitask-view/aux-views-data-samples/


SRC=$1 #Path to the directory containing the code
RSTDT_bracket=$2 #Path to the directory containing the bracketed files (.dmrg) 
OUTDIR_main=$3 #Path to the directory where the data for the main task are written 
OUTDIR_aux=$4 #Path to the directory where the data for the auxiliary views are written

OUTDIR_aux_nuc=${OUTDIR_aux}/const-nuc #Auxiliary view: only nuclearity kept
OUTDIR_aux_rel=${OUTDIR_aux}/const-rel #Auxiliary view: only relation kept
OUTDIR_aux_dep=${OUTDIR_aux}/const-dep #Auxiliary view: dependency format

mkdir -p $OUTDIR_main
mkdir -p $OUTDIR_aux
mkdir -p $OUTDIR_aux_rel
mkdir -p $OUTDIR_aux_nuc
mkdir -p $OUTDIR_aux_dep

# Main task: constituent form with relation and nuclearity
echo Write data: main task
python ${SRC}convert_rsttrees.py --inpath $RSTDT_bracket --outpath $OUTDIR_main --mode const

# Aux view: constituent form without relation but with nuclearity
echo Write data: aux view Nuc 
python ${SRC}convert_rsttrees.py --inpath $RSTDT_bracket --outpath $OUTDIR_aux_nuc --mode const --no_rel

# Aux view: constituent form without nuclearity but with relation
echo Write data: aux view Rel
python ${SRC}convert_rsttrees.py --inpath $RSTDT_bracket --outpath $OUTDIR_aux_rel --mode const --no_nuc 

# Aux view: dependency form  with relation and nuclearity
echo Write data: aux view Dep
python ${SRC}convert_rsttrees.py --inpath $RSTDT_bracket --outpath $OUTDIR_aux_dep --mode dep 

echo Done
