#!/bin/bash

# convert Factbank/Ontonotes/Speech corpora to sequences
# output files format:
#   sentence \t label \t id-sentence

SRC=$1 #Path to the directory containing the code
FACTBANK=$2 #Path to Factbank, ie factbank_v1/data/
ONTONOTE=$3 #Path to Ontonotes, ie ontonotes/english/
SPEECH=$4 #Path to Santa Barbara
OUTDIR=$5 #Path to the output directory

# SRC=/Users/chloebraud/projects/navi/sandbox/multitask-view/
# FACTBANK=/Users/chloebraud/projects/navi/sandbox/multitask-view/data-multitask/factbank_v1/data/
# ONTONOTE=/Users/chloebraud/projects/navi/sandbox/multitask-view/data-multitask/ontonotes/english/
# SPEECH=data/speech/
# OUTDIR=/Users/chloebraud/projects/navi/sandbox/multitask-view/aux-tasks/

mkdir -p $OUTDIR

# Factbank (and Timebank) data
echo Read Factbank/Timebank
python ${SRC}convert_auxtasks.py --annot $FACTBANK --outdir $OUTDIR --datatype factbank  

# Coreference from Ontonote
echo Read Ontonotes
python ${SRC}convert_auxtasks.py --annot $ONTONOTE --outdir $OUTDIR --datatype coref

# Talk turn from Santa Barbara Corpus
# The original data used have been pre-processed: 
# one line contains a piece of text and the name of a speaker:
#   Well I went to this	FRAN
#   and this is very	FRAN
#   is	SEAN
#   the	FRAN
echo Read Santa Barbara
python ${SRC)/convert_auxtasks.py --annot $SPEECH --outdir $OUTDIR --datatype speech

echo Done
