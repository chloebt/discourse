# README #

This repository contains the code necessary to replicate the experiments presented in: 

    C. Braud, B. Plank and A. S�gaard. Multi-view and multi-task training of RST discourse parsers, In Proceedings of COLING. 2016

### Required packages ###
* Python3
* CNN/pycnn http://github.com/yoavg/cnn/
* NLTK
* For pre-processing: xml.etree.ElementTree 

### Data ###
* RST Discourse Treebank
* OntoNotes
* TimeBank
* Santa Barbara Corpus
* Penn Discourse TreeBank

We also used Polyglot as pre-trained embeddings: <https://sites.google.com/site/rmyeid/projects/polyglot>.

Samples of the data used can be found in:

* data/main-task/: the RST trees converted to sequences, corresponding to the main task
* data/aux-views/: the auxiliary views, that is different representation of the main task, either keeping only nuclearity (/const-nuc/) or relation (/const-rel/) information, or representing the trees as dependency graphs (/const-dep/).
* data/aux-tasks/: the auxiliary tasks, i.e. sequence prediction tasks from the corpora cited above.

### Data preparation ###

The parser does sequence prediction tasks at the document level. It takes as input one file per document.

* RST trees have to be converted into sequences, and the system use other views of the RST trees. To generate these data, the RST trees (*.dis* files) have first to be transformed into a bracketed format (*.dmrg*), which can be done using the code in the parent directory *discourse/preprocess_rst/* (see README in this directory). Then, to obtain the files for the main task and the auxiliary views, use the following script:

        bash coling16/code/convert_rsttrees.sh PATH_SRC_DIR PATH_DMRG_DIR PATH_MAIN_TASK_DIR PATH_AUX_VIEWS_DIR
        
Samples of the data for the main task and the auxiliary views can be found resp. in *coling16/data/main-task/* and *coling16/data/aux-views/*.

* The system uses auxiliary tasks, i.e. sequential representations for the other datasets. To generate these data, use the following line:

        bash coling16/code/convert_auxtasks.sh PATH_SRC_DIR PATH_FACTBANK_DIR PATH_ONTONOTE_DIR PATH_BARBARA_DIR PATH_AUX_TASKS_DIR

Samples of the data for the auxiliary tasks can be found in *coling16/data/aux-tasks/*.

### Training a discourse parser ###

* Best set of parameters: 20 iterations, 2 layers, 200 dimensions, sigma 0.2
* Best combination of tasks: main+nuc+lab+dep+mod+pdtb

To train the parser with our best parameters, you can use the script *run_discomt.sh*. Change the path to your files in this script.
The script will also evaluate the predicted trees against the gold trees provided in the test directory.
You can test the code with the samples of data provided in this repository with the following line:

        bash path_to/coling16/code/run_discomt.sh path_to/coling16/code/src/ path_to/coling16/data/ output_discomt/

With this (really small) set of data, you should obtain the following scores for both models (span,nuc,rel): 60.42	34.07	22.88

### Evaluation ###

The evaluation script *parser_scorer.py* use code from [DPLP](https://github.com/jiyfeng/DPLP) to compute scores, and it also implements the heuristics described in our paper to retrieve well-formed trees based on the sequences predicted by the parser.
To compute a score, use the following line:

        python coling16/code/parser_scorer.py --test PATH_TEST_DIR --cpred PATH_PRED_FILE

### Contact ###
chloe.braud@gmail.com

