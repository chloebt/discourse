In an age of specialization , the federal judiciary is one of the last bastions of the generalist .	(Elaboration(Contrast(Explanation
A judge must jump from murder to antitrust cases , from arson to securities fraud ,	(Manner-Means
without missing a beat .	Manner-Means))
But even on the federal bench , specialization is creeping in ,	(Elaboration
and it has become a subject of sharp controversy on the newest federal appeals court .	Elaboration))
The Court of Appeals for the Federal Circuit was created in 1982	(Background(Enablement
to serve , among other things , as the court of last resort for most patent disputes .	Enablement)
Previously , patent cases moved through the court system to one of the 12 circuit appeals courts .	(Elaboration
There , judges	(Topic-Comment(Same-unit(Elaboration
who saw few such cases	(Joint
and had no experience in the field	Joint))
grappled with some of the most technical and complex disputes imaginable .	Same-unit)
A new specialty court was sought by patent experts ,	(Elaboration(Elaboration
who believed	(Attribution
that the generalists had botched too many important , multimillion-dollar cases .	Attribution))
Some patent lawyers had hoped	(Background(Contrast(Attribution
that such a specialty court would be filled with experts in the field .	Attribution)
But the Reagan administration thought otherwise ,	(Joint
and so may the Bush administration .	Joint))
Since 1984 , the president has filled four vacancies in the Federal Circuit court with non-patent lawyers .	(Topic-Comment(Elaboration(Elaboration
Now only three of the 12 judges	(Elaboration(Same-unit(Elaboration
-- Pauline Newman , Chief Judge Howard T. Markey , 68 , and Giles Rich , 85 --	Elaboration)
have patent-law backgrounds .	Same-unit)
The latter two and Judge Daniel M. Friedman , 73 , are approaching senior status or retirement .	Elaboration))
Three seats currently are vacant	(Joint
and three others are likely to be filled within a few years ,	Joint))
so patent lawyers and research-based industries are making a new push	(Elaboration(Elaboration
for specialists to be added to the court .	Elaboration)
Several organizations ,	(Elaboration(Elaboration(Same-unit(Elaboration
including the Industrial Biotechnical Association and the Pharmaceutical Manufacturers Association ,	Elaboration)
have asked the White House and Justice Department to name candidates with both patent and scientific backgrounds .	Same-unit)
The associations would like the court to include between three and six judges with specialized training .	Elaboration)
Some of the associations have recommended Dr. Alan D. Lourie , 54 , a former patent agent with a doctorate in organic chemistry	(Elaboration(Elaboration(Elaboration
who now is associate general counsel with SmithKline Beckman Corp. in Philadelphia .	Elaboration)
Dr. Lourie says	(Attribution
the Justice Department interviewed him last July .	Attribution))
Their effort has received a lukewarm response from the Justice Department .	(Contrast(Elaboration(Explanation
`` We do not feel	(Attribution(Attribution
that seats are reserved	(Elaboration
( for patent lawyers ) , ''	Elaboration))
says Justice spokesman David Runkel ,	(Elaboration
who declines to say	(Attribution
how soon a candidate will be named .	(Elaboration
`` But we will take it into consideration . ''	Elaboration)))))
The Justice Department 's view is shared by other lawyers and at least one member of the court , Judge H. Robert Mayer , a former civil litigator	(Explanation(Temporal(Elaboration
who served at the claims court trial level	Elaboration)
before he was appointed to the Federal Circuit two years ago .	Temporal)
`` I believe	(Elaboration(Attribution(Attribution
that any good lawyer should be able to figure out and understand patent law , ''	Attribution)
Judge Mayer says ,	Attribution)
adding	(Elaboration
that `` it 's the responsibility of highly paid lawyers	(Same-unit(Elaboration
( who argue before the court )	Elaboration)
to make us understand	(Elaboration
( complex patent litigation ) . ''	Elaboration))))))
Yet some lawyers point to Eli Lilly & Co. vs. Medtronic , Inc. , the patent infringement case the	(Elaboration(Elaboration(Elaboration(Explanation
Supreme Court this month agreed to review , as an example of poor legal reasoning by judges	(Elaboration
who lack patent litigation experience .	Elaboration))
( Judge Mayer was not on the three-member panel . )	Elaboration)
In the Lilly case , the appeals court broadly construed a federal statute	(Elaboration(Elaboration
to grant Medtronic , a medical device manufacturer , an exemption	(Enablement
to infringe a patent under certain circumstances .	Enablement))
If the Supreme Court holds in Medtronic 's favor ,	(Condition
the decision will have billion-dollar consequences for the manufacturers of medical devices , color and food additives and all other non-drug products	(Elaboration
that required Food & Drug Administration approval .	Elaboration))))
Lisa Raines , a lawyer and director of government relations for the Industrial Biotechnical Association , contends	(Elaboration(Elaboration(Attribution
that a judge well-versed in patent law and the concerns of research-based industries would have ruled otherwise .	Attribution)
And Judge Newman , a former patent lawyer , wrote in her dissent	(Attribution
when the court denied a motion for a rehearing of the case by the full court ,	(Cause
`` The panel 's judicial legislation has affected an important high-technological industry ,	(Manner-Means
without regard to the consequences for research and innovation or the public interest . ''	Manner-Means))))
Says Ms. Raines ,	(Attribution
`` ( The judgment )	(Same-unit
confirms our concern	(Elaboration
that the absence of patent lawyers on the court could prove troublesome . ''	Elaboration))))))))))))))))
