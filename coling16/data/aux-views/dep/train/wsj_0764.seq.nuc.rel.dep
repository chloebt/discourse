Energetic and concrete action has been taken in Colombia during the past 60 days against the mafiosi of the drug trade ,	root
but it has not been sufficiently effective ,	10_SN-Background
because , unfortunately , it came too late .	-1_NS-Explanation
Ten years ago , the newspaper El Espectador ,	4_SN-Background
of which my brother Guillermo was editor ,	-1_NS-Elaboration
began warning of the rise of the drug mafias and of their leaders ' aspirations	-2_NN-Same-unit
to control Colombian politics , especially the Congress .	-1_NS-Elaboration
Then ,	-6_NS-Explanation
when it would have been easier to resist them ,	1_SN-Contrast
nothing was done	-2_NN-Same-unit
and my brother was murdered by the drug mafias three years ago .	-1_NS-Cause
The most ruthless dictatorships have not censored their press more brutally than the drug mafias censor Colombia 's .	13_SN-Evaluation
The censorship is enforced through terrorism and assassination .	-1_NS-Manner-Means
In the past 10 years about 50 journalists have been silenced forever , murdered .	-2_NS-Explanation
Within the past two months a bomb exploded in the offices of the El Espectador in Bogota ,	-1_NN-Joint
destroying a major part of its installations and equipment .	-1_NS-Cause
And only last week the newspaper Vanguardia Liberal in the city of Bucaramanga was bombed ,	-2_NN-Joint
and its installations destroyed .	-1_NS-Cause
Journalists and their families are constantly threatened	-5_NS-Summary
as are the newspaper distribution outlets .	-1_NN-Joint
Distribution centers are bombed ,	-2_NN-Joint
and advertisers are intimidated .	-1_NN-Joint
Censorship is imposed by terrorism .	-11_NS-Summary
If the Colombian media accept this new and hideous censorship	1_SN-Condition
that the drug mafia 's terrorism someday will extend to all the newspapers	-1_NS-Elaboration
published in the free world .	-1_NS-Elaboration
The solidarity of the uncensored media world-wide against drug terrorism is the only way	-3_NN-Topic-Comment
press freedom can survive .	-1_NS-Elaboration
The American people and their government also woke up too late to the menace	-5_NN-Topic-Change
drugs posed to the moral structure of their country .	-1_NS-Elaboration
Even now , the American attack upon this tremendous problem is timid in relation to the magnitude of the threat .	-2_NS-Elaboration
I can attest	-1_NS-Elaboration
that a recent Colombian visitor to the U.S. was offered drugs three times in the few blocks ' walk between Grand Central Terminal and the Waldorf Astoria Hotel in midtown Manhattan .	-1_NS-Attribution
Colombia alone	-5_NN-Topic-Change
-- its government , its people , its newspapers --	-1_NS-Elaboration
does not have the capacity	-2_NN-Same-unit
to fight this battle successfully .	-1_NS-Elaboration
All drug-consuming countries must jointly decide to combat and punish the consumers and distributors of drugs .	-4_NN-Topic-Comment
The U.S. , as the major drug consumer , should lead this joint effort .	-1_NS-Elaboration
Reduction , if not the total cessation , of drug consumption is the requirement for victory .	-6_NS-Evaluation
Much is being done in Colombia	-7_NS-Elaboration
to fight the drug cartel mafia .	-1_NS-Enablement
Luxurious homes and ranches have been raided by the military authorities ,	-2_NS-Explanation
and sophisticated and powerful communications equipment have been seized .	-1_NN-Joint
More than 300 planes and helicopters have been impounded at airports ,	-2_NN-Joint
and a large number of vehicles and launches has been confiscated .	-1_NN-Joint
The military has also captured enormous arsenals of powerful and sophisticated weapons , explosives and other war-like materiel .	-2_NN-Joint
Much has been accomplished	2_SN-Contrast
and public opinion decisively supports the government and the army	-1_NS-Elaboration
-- but , on the other hand , none of the key drug barons have been captured .	-9_NS-Elaboration
There has been a lot of talk	11_SN-Evaluation
that a large portion of the Colombian economy is sustained by the laundering of drug money .	-1_NS-Elaboration
In my opinion , this is not true .	-2_NS-Evaluation
Laundered drug money has served only to increase , unrealistically , the price of real estate ,	6_SN-Contrast
creating serious problems for low-income people	-1_NS-Cause
who aspire to own their own homes .	-1_NS-Elaboration
Drug money has also gone	-3_NN-Joint
to buy expensive cars , airplanes , launches and nightclubs	-1_NS-Enablement
where drugs are consumed .	-1_NS-Elaboration
But most of the drug money is kept in investments and in financial institutions outside Colombia .	-7_NS-Explanation
In fact , the cooperation of those financial institutions is essential to the success of the drug battle .	-1_NS-Elaboration
What is of much more importance to the Colombian economy than the supposed benefits of laundered drug money is higher prices for Colombia 's legitimate products .	-21_NS-Elaboration
The price of coffee has gone down almost 45 % since the beginning of the year , to the lowest level	-1_NS-Explanation
( after inflation )	-1_NS-Elaboration
since the Great Depression .	-2_NN-Same-unit
Market conditions point to even lower prices next year .	-3_NS-Elaboration
The 27-year-old coffee cartel had to be formally dissolved this summer .	1_SN-Cause
As a result , Colombia will earn $ 500 million less from its coffee this year than last .	-5_NN-Joint
Our coffee growers face reductions in their income ,	1_SN-Cause
and this tempts them to contemplate substituting coca crops for coffee .	-2_NN-Joint
U.S. interests occasionally try to impose barriers to the import of another important Colombian export	-8_NN-Joint
-- cut flowers --	-1_NS-Elaboration
into the American market .	-2_NN-Same-unit
A just price and an open market	-3_NN-Topic-Comment
for what Colombian produces and exports	-1_NS-Elaboration
should be the policy of the U.S .	-2_NN-Same-unit
I take advantage of this opportunity	-53_NS-Evaluation
given to me by The Wall Street Journal	-1_NS-Elaboration
to make a plea to the millions of readers of this newspaper ,	-2_NS-Enablement
to become soldiers	-1_NS-Elaboration
dedicated to the fight against the use of drugs .	-1_NS-Elaboration
Each gram of cocaine consumed is a deadly bullet against those in our country and in the rest of the world	-5_NS-Elaboration
who fight this terrible scourge .	-1_NS-Elaboration
A crusade of NO to the consumption of drugs is imperative .	-2_NN-Joint
Mr. Cano is president of El Espectador , a newspaper	-61_NN-Textual-organization
founded by	-1_NS-Elaboration
