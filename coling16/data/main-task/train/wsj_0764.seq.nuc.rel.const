Energetic and concrete action has been taken in Colombia during the past 60 days against the mafiosi of the drug trade ,	(NN-Textual-organization(NS-Evaluation(NN-Topic-Change(SN-Evaluation(SN-Background(NS-Explanation(SN-Contrast
but it has not been sufficiently effective ,	(NS-Explanation
because , unfortunately , it came too late .	NS-Explanation))
Ten years ago , the newspaper El Espectador ,	(SN-Background(NN-Same-unit(NS-Elaboration
of which my brother Guillermo was editor ,	NS-Elaboration)
began warning of the rise of the drug mafias and of their leaders ' aspirations	(NS-Elaboration
to control Colombian politics , especially the Congress .	NS-Elaboration))
Then ,	(NN-Same-unit
when it would have been easier to resist them ,	(SN-Contrast
nothing was done	(NS-Cause
and my brother was murdered by the drug mafias three years ago .	NS-Cause)))))
The most ruthless dictatorships have not censored their press more brutally than the drug mafias censor Colombia 's .	(NS-Summary(NS-Explanation(NS-Manner-Means
The censorship is enforced through terrorism and assassination .	NS-Manner-Means)
In the past 10 years about 50 journalists have been silenced forever , murdered .	(NS-Summary(NN-Joint
Within the past two months a bomb exploded in the offices of the El Espectador in Bogota ,	(NN-Joint(NS-Cause
destroying a major part of its installations and equipment .	NS-Cause)
And only last week the newspaper Vanguardia Liberal in the city of Bucaramanga was bombed ,	(NS-Cause
and its installations destroyed .	NS-Cause)))
Journalists and their families are constantly threatened	(NN-Joint(NN-Joint
as are the newspaper distribution outlets .	NN-Joint)
Distribution centers are bombed ,	(NN-Joint
and advertisers are intimidated .	NN-Joint))))
Censorship is imposed by terrorism .	NS-Summary))
If the Colombian media accept this new and hideous censorship	(NN-Topic-Comment(SN-Condition
there is little doubt	(NS-Elaboration
that the drug mafia 's terrorism someday will extend to all the newspapers	(NS-Elaboration
published in the free world .	NS-Elaboration)))
The solidarity of the uncensored media world-wide against drug terrorism is the only way	(NS-Elaboration
press freedom can survive .	NS-Elaboration)))
The American people and their government also woke up too late to the menace	(NN-Topic-Change(NS-Elaboration(NS-Elaboration
drugs posed to the moral structure of their country .	NS-Elaboration)
Even now , the American attack upon this tremendous problem is timid in relation to the magnitude of the threat .	(NS-Elaboration
I can attest	(NS-Attribution
that a recent Colombian visitor to the U.S. was offered drugs three times in the few blocks ' walk between Grand Central Terminal and the Waldorf Astoria Hotel in midtown Manhattan .	NS-Attribution)))
Colombia alone	(NS-Elaboration(NS-Evaluation(NN-Topic-Comment(NN-Same-unit(NS-Elaboration
-- its government , its people , its newspapers --	NS-Elaboration)
does not have the capacity	(NS-Elaboration
to fight this battle successfully .	NS-Elaboration))
All drug-consuming countries must jointly decide to combat and punish the consumers and distributors of drugs .	(NS-Elaboration
The U.S. , as the major drug consumer , should lead this joint effort .	NS-Elaboration))
Reduction , if not the total cessation , of drug consumption is the requirement for victory .	NS-Evaluation)
Much is being done in Colombia	(NS-Elaboration(NS-Elaboration(NS-Explanation(NS-Enablement
to fight the drug cartel mafia .	NS-Enablement)
Luxurious homes and ranches have been raided by the military authorities ,	(NN-Joint(NN-Joint
and sophisticated and powerful communications equipment have been seized .	NN-Joint)
More than 300 planes and helicopters have been impounded at airports ,	(NN-Joint(NN-Joint
and a large number of vehicles and launches has been confiscated .	NN-Joint)
The military has also captured enormous arsenals of powerful and sophisticated weapons , explosives and other war-like materiel .	NN-Joint)))
Much has been accomplished	(SN-Contrast(NS-Elaboration
and public opinion decisively supports the government and the army	NS-Elaboration)
-- but , on the other hand , none of the key drug barons have been captured .	SN-Contrast))
There has been a lot of talk	(NS-Explanation(SN-Evaluation(NS-Evaluation(NS-Elaboration
that a large portion of the Colombian economy is sustained by the laundering of drug money .	NS-Elaboration)
In my opinion , this is not true .	(NS-Explanation
Laundered drug money has served only to increase , unrealistically , the price of real estate ,	(SN-Contrast(NN-Joint(NS-Cause
creating serious problems for low-income people	(NS-Elaboration
who aspire to own their own homes .	NS-Elaboration))
Drug money has also gone	(NS-Enablement
to buy expensive cars , airplanes , launches and nightclubs	(NS-Elaboration
where drugs are consumed .	NS-Elaboration)))
But most of the drug money is kept in investments and in financial institutions outside Colombia .	(NS-Elaboration
In fact , the cooperation of those financial institutions is essential to the success of the drug battle .	NS-Elaboration))))
What is of much more importance to the Colombian economy than the supposed benefits of laundered drug money is higher prices for Colombia 's legitimate products .	SN-Evaluation)
The price of coffee has gone down almost 45 % since the beginning of the year , to the lowest level	(NN-Joint(NN-Joint(NS-Elaboration(NN-Same-unit(NS-Elaboration
( after inflation )	NS-Elaboration)
since the Great Depression .	NN-Same-unit)
Market conditions point to even lower prices next year .	NS-Elaboration)
The 27-year-old coffee cartel had to be formally dissolved this summer .	(NN-Joint(SN-Cause
As a result , Colombia will earn $ 500 million less from its coffee this year than last .	SN-Cause)
Our coffee growers face reductions in their income ,	(SN-Cause
and this tempts them to contemplate substituting coca crops for coffee .	SN-Cause)))
U.S. interests occasionally try to impose barriers to the import of another important Colombian export	(NN-Topic-Comment(NN-Same-unit(NS-Elaboration
-- cut flowers --	NS-Elaboration)
into the American market .	NN-Same-unit)
A just price and an open market	(NN-Same-unit(NS-Elaboration
for what Colombian produces and exports	NS-Elaboration)
should be the policy of the U.S .	NN-Same-unit))))))))
I take advantage of this opportunity	(NS-Elaboration(NS-Enablement(NS-Elaboration
given to me by The Wall Street Journal	NS-Elaboration)
to make a plea to the millions of readers of this newspaper ,	(NS-Elaboration
to become soldiers	(NS-Elaboration
dedicated to the fight against the use of drugs .	NS-Elaboration)))
Each gram of cocaine consumed is a deadly bullet against those in our country and in the rest of the world	(NN-Joint(NS-Elaboration
who fight this terrible scourge .	NS-Elaboration)
A crusade of NO to the consumption of drugs is imperative .	NN-Joint)))
Mr. Cano is president of El Espectador , a newspaper	(NS-Elaboration
founded by	NS-Elaboration))
