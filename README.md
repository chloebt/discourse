# README #

This repository contains code and data for discourse parsing:

* coling16/ contains the code used in: C. Braud, B. Plank and A. S�gaard. Multi-view and multi-task training of RST discourse parsers, In Proceedings of COLING. 2016
* eacl17-discourseParsing/ contains the code used in: C. Braud, M. Coavoux and A. S�gaard. Cross-lingual RST Discourse Parsing, In Proceedings of EACL. 2017 code for pre-processing the data is still a bit messy, don't hesitate to send an email if you have any question!
* preprocess_rst/ contains the code to produce bracketed files (possibly, with a mapping of all the relations) from all the corpora used in the EACL17 experiments.
* acl17-discourseSegmenter/ contains the code used in: C. Braud, O. Lacroix and A. S�gaard. Cross-lingual and cross-domain discourse segmentation of entire documents, In Proceedings of ACL. 2017 (Coming soon)
* emnlp17-discourseSegmenter/ contains the code used in:  C. Braud, O. Lacroix and A. S�gaard. Does syntax help discourse segmentation? Not so much, In Proceedings of EMNLP. 2017 (Coming soon)