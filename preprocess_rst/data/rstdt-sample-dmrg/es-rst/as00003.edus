Cúmulos de galaxias próximos. Propiedades globales y caracterización de sus poblaciones galácticas .
Los Cúmulos de Galaxias son una fuente inagotable de información que abarca desde la formación y evolución de galaxias hasta aspectos a nivel cosmológico .
Sin embargo , hasta no hace mucho tiempo , se daba la paradoja de que era más fácil estudiar Cúmulos de Galaxias lejanos que no aquellos que se encuentran más próximos a nosotros .
Este problema meramente técnico está siendo superado gracias a la nueva generación de Cámara y de Gran Campo que aunan las ventajas tecnológicas de las cámaras CCD con campos de visión cada vez mayores .
Gracias a ellos en la actualidad se pueden llevar a cabo proyectos de gran envergadura dirigidos a cubrir esas laguna de conocimiento sobre los Cúmulos de Galaxias más próximos ,
lo que permitirá tener una visión completa de los mismos sobre la que apoyar los estudios de Cúmulos más lejanos y , por lo tanto , en etapas de su evolución más tempranas .
El proyecto WINGS ( Wide-field Image Nearby-clusters Survey ) tiene como objetivo principal precisamente el establecer un marco de referencia para todo tipo de estudios centrados en Cúmulos de Galaxias , pero muy especialmente para estudios evolutivos los cuales hasta este momento carecían de una muestra lo suficientemente amplia de Cúmulos de Galaxias próximos con la que comparar y poder distinguir entre efectos evolutivos y variaciones intrínsecas de las propiedades de los Cúmulos .
En esta Tesis se presentan los aspectos fundamentales de la extracción y análisis de los datos obtenidos en el proyecto WINGS .
Conocer , en la mitad de lo posible , aquellos factores ,
ya sean de origen técnico ,
debidos a proyección o locales de cada cúmulos , que pueden influir en la distribución delos valores de los parámetros característicos de los Cúmulos de Galaxias es necesario
para que las conclusiones derivadas de estos datos estén libres de efectos espurios .
En este sentido , no sólo se han estudiado los métodos de extracción de los datos sino también aquellos aspectos fundamentales en cualquier estudio en profundidad de un Cúmulo de Galaxias
como lo son el contenido en tipos morfológicos , la Función de Luminosidad , la Relación Color-Magnitud , la Relación de Kormendy y la distribución espacial de las galaxias en los Cúmulos .
